<!DOCTYPE html>
<html>
<head>
	<title>Validate Password</title>

	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  	<!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body style="">
	<header class="card-panel">Ozze	</header>
	<section>
		<div class="container">
			<div class="valign-wrapper" style="height:500px;">
				<div class='valign'>
					<div>
						<h4>Validation Code</h4><br>
						<form>
							<div class="input-field col s10">
								<input id='passcode_' type="password" name="_validate">
								<label class="active" for="passcode_">Password</label>
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<button>Validate</button>
							</div>
						</form>
						
					</div>
					
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

    <script type="text/javascript">
    	
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
        
    </script>

</body>
</html>