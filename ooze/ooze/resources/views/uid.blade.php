<!DOCTYPE html>
<html>
<head>
	<title>Create UID</title>

	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  	<!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body style="">
	<header class="card-panel">Ozze	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col s6">
					<div class="valign-wrapper"  style="height: 500px;">
						<div class="card valign" style="height: 250px; width:400px;padding:15px;">
							<div class="input-field">
								<form>
									<input type="text" name="_uid" id='_uid'>
									<input type="hidden" value="{{csrf_token()}}" name="_token">
									<label class="active" for="_uid">Unique ID</label>
									<button>Create UID</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col s6" style="border-left: 1px solid gainsboro; padding:15px;">
					<div class="valign-wrapper" style="height:500px;">
						<div class="valign">
							<h3>card view</h3>
						</div>
					</div>
				</div>

			</div>
		</div>

	</section>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

    <script type="text/javascript">
    	
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
        
    </script>

</body>
</html>