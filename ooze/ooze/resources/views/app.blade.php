<!DOCTYPE html>
<html>
<head>
	<title>Inside Ooze</title>

	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  	<!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body style="">
	<header class="card-panel">Ozze	</header>
	<section>
		<div class="">
			<div class="row">
				<aside class="col s3" style="border-left: 1px solid gainsboro; padding:15px;">
					<ul class="collection">
						<li class="collection-item">+ Invite People</li>
						<li class="collection-item">Profile and acount</li>
						<li class="collection-item">Give a shout out</li>
						<li class="collection-item">Manage Contacts</li>
					</ul>

					
				  <ul class="collection">
				    <li class="collection-item avatar">
				      <img src="images/yuna.jpg" alt="" class="circle">
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle">folder</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle green">insert_chart</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle red">play_arrow</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				  </ul>
            
				</aside>
				<section class="col s9" style="border-left: 1px solid gainsboro; padding:15px;">
					<div class="card section" style="min-height: 10px">
							
							<div class='valign-wrapper' style="height: 10px; width: 100%;padding:7%;">
								<div class='valign col s12'>
									<div class="col s12" style="">
										<form>
											<div class="input-field">
												<div class="col s9" style="">
													<input id="_speakup" type="text" name="" style="height: 80px;width: 100%">
													<label class="active" for="_speakup">Hey! what is it on your mind?</label>
												</div>
												<div class="col s3" style="">
													<button>ooze</button>
												</div>
											</div>
										</form>
										
									</div>
								</div>
							</div>
						
					</div>
					<div class='divider'></div>
					<div class="col s12">
						<!-- <div class="row">
							<div class="col s4" style="height: 300px; border-right: 1px solid gainsboro">
								sjs
							</div>
							<div class="col s8">
								sjs
							</div>
						</div> -->
					</div>
					<ul class="collection">
				    <li class="collection-item avatar">
				      <img src="images/yuna.jpg" alt="" class="circle">
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle">folder</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle green">insert_chart</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				    <li class="collection-item avatar">
				      <i class="material-icons circle red">play_arrow</i>
				      <span class="title">Title</span>
				      <p>First Line <br>
				         Second Line
				      </p>
				      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
				    </li>
				  </ul>
				</section>

			</div>
		</div>

	</section>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

    <script type="text/javascript">
    	
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
        
    </script>

</body>
</html>