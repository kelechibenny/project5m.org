<!DOCTYPE html>
<html>
<head>
	<title>Ozze</title>

	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  	<!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body style="">
	<header class="card-panel">Ozze	</header>
	<section>
		<div style="padding: 0px 40px; min-height:500px;">
			<div class='row'>
				<div class="col s6 valign-wrapper" style="height:500px;">
					
					<div class='valign'>
						<h3 >Wake up and say HI</h3>
						<!-- <input type="email" name="init_">
						<button>Create App</button>
	 -->
						<div class="input-field col s10">
							<form method='POST' action='/'>
								<!-- <i class="material-icons prefix">phone</i> -->
						     	<input value="" id="init_" type="email" name='init_value' class="validate" placeholder="Email address">
						     	<label class="active" for="init_" data-error="Wrong email format" data-success="Correct!">Email</label>
						     	<input type="hidden" value="{{csrf_token()}}" name="_token">
						     	<button type="submit" name='app_create'>Create app</button>
						     </form>
					    </div>

					</div>
				</div>
				<div class="col s6 card-panel" style="height:500px;">
					
					<div class="container valign-wrapper" style="height:inherit">
					  	<div>
					  		<h4 class="valign">Talk Ooze</h4>
					  		<p>Ther are moments of ou lives that we miss. They vital moments when all we feel is what matters. Thise moments when we are happy and then times of sadness. If we can only ooze of with friends.</p>
					  		<h5>Share the moments. Ooze with one another</h5>
					  	</div>
					</div>
        
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

    <script type="text/javascript">
    	
  $(document).ready(function() {
    Materialize.updateTextFields();
  });
        
    </script>

</body>
</html>