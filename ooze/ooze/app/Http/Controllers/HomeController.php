<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function home()
    {
    	return view('home');
    }

    public function validate_()
    {
    	return view('validate');
    }

    public function uid_()
    {
    	return view('uid');
    }

    public function app_()
    {
    	return view('app');
    }
}
