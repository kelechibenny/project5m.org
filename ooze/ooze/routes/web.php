<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@home');

Route::post('/', 'HomeController@validate_');

// Route::get('/validate', 'HomeController@validate_');

Route::post('/validate', 'HomeController@uid_');

Route::get('/app', 'HomeController@app_');

Route::post('/app', 'HomeController@app_');
