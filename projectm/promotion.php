<?php 
session_start();
if(!isset($_SESSION['id'])) header("location: index.php");
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Promotion</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a> 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	// $refererController = new RefererController;
	// $userController = new UserController;

	$users = $userModel->getUserDetail($_SESSION['id']);
	// var_dump($users[0]); die;
	extract($users[0]);
	$myemail = $email;
				
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10 '>
	
		<div class='col-sm-5 card' style="margin:18px; min-height:150px;">Banner1 <a href='bootstrap/promotions/banner1.php'><button>Download</button></a></div>
		<div class='col-sm-5 card' style="margin:18px; min-height:150px;">Banner2 <a href='bootstrap/promotions/banner2.php'><button>Download</button></a></div>
		<div class='col-sm-5 card' style="margin:18px; min-height:150px;">Banner3 <a href='bootstrap/promotions/banner3.php'><button>Download</button></a></div>
		<div class='col-sm-5 card' style="margin:18px; min-height:150px;">Banner4 <a href='bootstrap/promotions/banner4.php'><button>Download</button></a></div>
		<div class='col-sm-5 card' style="margin:18px; min-height:150px;">Banner5 <a href='bootstrap/promotions/banner1.php'><button>Download</button></a></div>
	
	</section>
</div>
<!-- <footer class="col-md-12">
Copyright © <?php //echo date('Y'); ?>. PROJECT5MILLION
</footer> -->

<?php
	//include "view/footer.php";
?>
</body>
</html>