<?php 
session_start();
if(!isset($_SESSION['id'])) header("location: index.php");
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Support</title>
	<meta charset='utf-8'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$refererController = new RefererController;
	$userController = new UserController;

	$users = $userModel->getUserDetail($_SESSION['id']);
	// var_dump($users[0]); die;
	extract($users[0]);
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10'>

	<!-- <div id="map" style="height:400px;"> -->
		
		<!-- <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.mx/?ie=UTF8&amp;ll=32.689593,-117.18039&amp;spn=0.023259,0.038495&amp;t=m&amp;z=15&amp;output=embed"></iframe>

		<div class="marker-wrapper">
			<div class="marker-icon"></div>
			<div class="marker"></div>
		</div> -->

		<!-- <div id="directions">
			<p>Get directions to our office</p>
			<form>
				<div class="form-group">
					<input class="form-control" type="text" placeholder="Write your zip code" />
				</div>
				<button type="submit" class="button button-small">
					<span>Get directions</span>
				</button>
			</form>
		</div> -->
	<!-- </div><br> -->

	<div id="info">
		<div class="container">
			<div class="row">
				<div class="col-md-8 message">
					<h3>Send us a message</h3>
					<p>
						You can contact us with anything related to Project5Million. <br/> We'll do all we can to resolve your issues.
					</p>

					<?php
						$userModel = new User;
						$users = $userModel->getUserDetail($_SESSION['id']);
						extract($users[0]);
					?>

					<form role="form" id="contact-form" method="post">
						<div class="form-group">
				    		<label for="email">Email address</label>
				    		<input type="email" name="email" class="form-control" id="email" value="<?php echo $email; ?>" readonly />
				  		</div>
				  		<div class="form-group">
				    		<label for="subject">Subject</label>
				    		<select class="form-control" id="subject" name="subject" required>
								<option value='1'>Blocked Account</option>
								<option value='2'>Confirmation Delay</option>
								<option value='3'>Project Ajdustment</option>
								<option value='4'>Transaction Spamming</option>
								<option value='5'>Other</option>
							</select>
				    	</div>
				  		<div class="form-group">
				    		<label for="message">Your message</label>
				    		<textarea name="message" class="form-control" id="message" rows="6" required></textarea>
				  		</div>
				  		<div class="submit">
				  			<input type="submit" class="button button-small" value="Submit" name="contact_admin" />
				  		</div>
					</form>
				</div>
				<div class="col-md-4 contact">
					<!-- <div class="address">
						<h3>Our Address</h3>
						<p>
							The Old Road Willington, <br />
							7 Kings Road, <br />
							Southshore, 64890
						</p>
					</div> -->
					<!--div class="phone">
						<h3>By Phone</h3>
						<p>
							234-813-846-4344
						</p>
					</div-->
					<div class="online-support">
						<strong>Looking for online support?</strong>
						<p>
							Talk to us now with our online chat
						</p>
					</div>
					<div class="social">
						<a href="#" class="fb"><img src="apps/images/social/fb.png" alt="facebook" /></a>
						<a href="#" class="tw"><img src="apps/images/social/tw.png" alt="twitter" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	</section>
</div>
<?php
	//include "view/footer.php";
?>	
</body>
</html>