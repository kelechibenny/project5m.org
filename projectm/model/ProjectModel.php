<?php

class ProjectModel{
	public $count = 0;

	public function __construct(){
		// $user = new User;
	}

	public function detail($cat = null, $user = null){
		$db = new Database;
		$sql = $db->connect();
		
		$where = is_null($user) ? null : "AND `user` = {$user} ";
		$gory = is_null($cat) ? null : "AND `projects`.`category` = '".$sql->real_escape_string($cat)."' ";
		//get from 
		$projects = array();
		$project = $sql->query("SELECT * FROM `projects`
									JOIN `projects_category`
										ON `projects`.`category` = `projects_category`.`id`
											JOIN `users`
												ON `users`.`id` = `projects`.`user`
													WHERE `projects_category`.`status` = '1' $gory  $where ");
		if($project->num_rows > 0){
			while($proj = $project->fetch_array()){
				 $projects[$this->count] = $proj;
				 $this->count++;
			}
			return $projects;
		}
		else return false;
	}

	public function category($category = null){
		$db = new Database;
		$sql = $db->connect();
		$where = is_null($category) ? null : "AND `category` = '".$sql->real_escape_string($category)."' ";
		//get from 
		$projects = array();
		$project = $sql->query("SELECT * FROM `projects_category` WHERE `projects_category`.`status` = '1' $where ");
		if($project->num_rows > 0){
			while($proj = $project->fetch_array()){
				 $projects[$this->count] = $proj;
				 $this->count++;
			}
			return $projects;
		}
	}
	
	
	public function category_admin(){
		$db = new Database;
		$sql = $db->connect();
		//get from 
		$projects = array();
		$project = $sql->query("SELECT * FROM `projects_category` ");
		if($project->num_rows > 0){
			while($proj = $project->fetch_array()){
				 $projects[$this->count] = $proj;
				 $this->count++;
			}
			return $projects;
		}
	}
}