<?php 
// include_once "bootstrap/Autoload.php";
class User{
	public $count = 0;

	public function getUserDetail($user = null, $level = null){
		// var_dump($user); echo "chek<br>";
		$where = ($user == null or $user== "") ? null : "WHERE `id` = {$user} ";
		$lvl = ($level == null or $level== "") ? null : "WHERE `level` = {$level} ";
		$db = new Database;
		$sql = $db->connect();
		$users = array();
		// var_dump($where); die;
		  $user= $sql->query("SELECT * FROM `users` $where $lvl ");
		  if($user->num_rows > 0) {
		  	while($userd = $user->fetch_assoc()){
		  		$users[$this->count] = $userd;
		  		$this->count++;
		  	}
		  	return $users;
		  }
		  // else return false;

	}

	public function getBlockedUsers(){
		$db = new Database;
		$sql = $db->connect();
		$users = array();
		// var_dump($where); die;
		  $user= $sql->query("SELECT * FROM `users` WHERE `block` = '1' ");
		  if($user->num_rows > 0) {
		  	while($userd = $user->fetch_assoc()){
		  		$users[$this->count] = $userd;
		  		$this->count++;
		  	}
		  	return $users;
		  }
		  // else return false;

	}

	public function referer($user = null){
		$where = is_null($user) ? null : "WHERE `id` = {$user}";
		$db = new Database;
		$sql = $db->connect();
		$refs = array();
		  $ref = $sql->query("SELECT `ref1`, `ref2`, `ref3`, `ref4` FROM `users` $where ");
		  if($ref->num_rows > 0) {
		  	// while($refd = $ref->fetch_assoc()){
		  	// 	$refs[$this->count] = $refd;
		  	// }
		  	// // echo "<pre>";
		  	// // var_dump($refs); 
		  	// return $refs;
		  	return $ref->fetch_assoc();
		  }
	}

	public function refered_detail($refered){
		//die($refered);
		$db = new Database;
		$sql = $db->connect();
		$refs = array();
		  $ref = $sql->query("SELECT * FROM `users` WHERE `id` = {$refered} ");
		  if($ref->num_rows > 0) {
		  	while($refd = $ref->fetch_assoc()){
		  		$refs[$this->count] = $refd;
		  	}
		  	return $refs;
		  }
	}

	public function level($level){
		//die($level);
		$db = new Database;
		$sql = $db->connect();
		$refs = array();
		  $ref = $sql->query("SELECT * FROM `levels` WHERE `id` = {$level} ");
		  if($ref->num_rows > 0) {
		  	return $ref->fetch_array();
		  }
		  else return array("7","Starter","5000","0");
	}

	public function paidBy($user, $ref = null, $limit = null, $level = null){
		$r = is_null($ref) ? null : "AND `paidby` = {$ref}";
		$lev = is_null($level) ? null : "AND `transactions`.`payment_level` = {$level}";
		$stack = array();
		$db = new Database;
		$sql = $db->connect();
		$trans = array();
		  $tran = $sql->query("SELECT * FROM `transactions`
		  							LEFT JOIN `users`
		  								ON `transactions`.`paidby` = `users`.`id`
		  									 LEFT JOIN `levels`
		  										ON `users`.`level` = `levels`.`id`
		  											WHERE `paidto` = {$user} $r $lev
		  												ORDER BY `transactions`.`id` DESC $limit ");
		  if($tran->num_rows > 0) 
		  	{
		  		while($t = $tran->fetch_array()){
		  			$stack[$this->count] = $t;
		  			//$s = $stack[0+$this->count];
		  			$this->count++;
		  		}
		  		return $stack;
		  	}
		  else return false;
	}

	public function paidTo($user, $ref = null, $limit = null, $level = null){
		$r = is_null($ref) ? null : "AND `paidto` = {$ref}";
		// die($r);
		$lev = is_null($level) ? null : "AND `transactions`.`payment_level` = {$level}";
		// die($lev);
		$stack = array();
		$db = new Database;
		$sql = $db->connect();
		$trans = array();
		  $tran = $sql->query("SELECT * FROM `transactions`
		  							LEFT JOIN `users`
		  								ON `transactions`.`paidto` = `users`.`id`
		  									LEFT JOIN `levels`
		  										ON `users`.`level` = `levels`.`id`
		  									 		WHERE `paidby`= {$user} $r $lev
		  									 			ORDER BY `transactions`.`id` DESC $limit  ");
		  if($tran->num_rows > 0) 
		  	{
		  		while($t = $tran->fetch_array()){
		  			$stack[$this->count] = $t;
		  			//$s = $stack[0+$this->count];
		  			$this->count++;
		  		}
		  		return $stack;
		  	}
		  else return false;
	}

	public function initialPaymentStatus($user, $ref, $limit = null){
		// var_dump($user); die($user);
		// echo "hr";
		$db = new Database;
		$sql = $db->connect();
		$trans = array();
		$tran = $sql->query("SELECT * FROM `transactions`
		  							WHERE `paidby` = {$user}
		  								AND `paidto` = {$ref}
		  									AND `payment_level`= '0'
		  										ORDER BY `transactions`.`id` DESC $limit ");
		  if($tran->num_rows > 0) return $tran->fetch_array();
		  else return false;
	}

	public function getLevel($level){
		$paylev = array();
		$db = new Database;
		$sql = $db->connect();
		$lev = $sql->query("SELECT `paidby` FROM `transactions` WHERE `payment_level` = {$level} ");
		if($lev->num_rows > 0) {
			while($l = $lev->fetch_assoc()){
				$paylev[$this->count] = $l;
				$this->count++;
			}
			return $paylev;
		}
		else return false;
	}

	public function blockUser($user){
		$db = new Database;
		$sql = $db->connect();
		$lev = $sql->query("UPDATE `users` SET `block`='1' WHERE `id` = '$user' ");
		if($lev) {
			$me = $this->getUserDetail($user)[0];
			// var_dump($user);
			$email = $me['email'];
			$name = $me['firstname']." ".$me['lastname'];
			//send mail to user about blocked status
			include "bootstrap/emails/blocked_email.php";
		}
		// else echo "error";
	}

	public function tempBlockUser($user){
		$db = new Database;
		$sql = $db->connect();
		$lev = $sql->query("UPDATE `users` SET `tempblock`='1' WHERE `id` = '$user' ");
		if($lev) {
			$me = $this->getUserDetail($user)[0];
			// var_dump($user);
			$email = $me['email'];
			$name = $me['firstname']." ".$me['lastname'];
			//send mail to user about blocked status
			include "bootstrap/emails/upgrade_email.php";
		}
		// else echo "error";
	}

	public function deleteUser($user){
		$db = new Database;
		$sql = $db->connect();
		$select_refby = $sql->query("SELECT `refby` FROM `users` WHERE `id` = '{$user}' ");
		$refby = $select_refby->fetch_assoc()['refby'];
		$select_ref = $sql->query("SELECT `ref1`, `ref2`, `ref3`, `ref4` FROM `users` WHERE `id` = '{$refby}' ");
		$ref = $select_ref->fetch_assoc();
		// var_dump($refby);
		// echo "<br>";
		if(is_array($ref)){
			$ke = array_search($user, $ref);
			$pop = $sql->query("UPDATE `users` SET `".$ke."` = ' ' WHERE `id` = '{$refby}' ");
		}
		$del = $sql->query("DELETE FROM `users` WHERE `id` = '{$user}' ");
		

		if($del){
			$me = $this->getUserDetail($user)[0];
			// var_dump($user);
			$email = $me['email'];
			$name = $me['firstname']." ".$me['lastname'];
			//send mail to user about delete status
			include "bootstrap/emails/deleted_email.php";
		}
	}


}