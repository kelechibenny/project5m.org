<?php

class RefererController{
	public $count = 0;
	public $userModel;
	public $userCon;

	public function __construct(){
		$this->userModel = new User;
		$this->userCon = new UserController;
	}

	public function check($refs , $referer=null){
		foreach ($refs as $key => $ref) {
			if($ref == 0) {
				//Empty Reference field (key) is retured
				//called from forms
				return array($key, $referer);
				break;
			}
		}
	}


	public function complete($key){
		//track user level //send back to user model //True means referer field is full
		if($key == null) {
			return true;
		}
		else return false;
	}

	public function detail($refs){
		$holddetail = array();
		foreach ($refs as $key => $ref) {
			$userModel = new User;
			$holddetail[$this->count] = $userModel->refered_detail($ref);
			$this->count++;
		}
		return $holddetail;
	}

	public function compareLevels($user, $refby){
		// var_dump($refby); die;
		$u = $this->userModel->getUserDetail($user);
		$user = $u[0]['level'];
		// echo $user;
		$u = $this->userModel->getUserDetail($refby);
		// var_dump($refby);
		// echo "<br>";
		// var_dump($u); 
		// // echo "<br>";

		$time = new TimeController;
		$d = $time->time(date("Y-m-d h:i:s"), $u[1]['level_date']);
		// $countdown = $time->countdown($d);
		// if($countdown == false && $refby != '1') $this->userCon->block($refby);


			if($user == 0){
				$countdown = $time->countdown($d);
				if($countdown == false && $refby != '1') $this->userCon->block($refby);
			}
			elseif($user == 1){
				$countdown = $time->fourWeeks($d);
				if($countdown == false && $refby != '1') $this->userCon->block($refby);
			}
			elseif($user > 1){
				$countdown = $time->countdown($d);
				if($countdown == false && $refby != '1') $this->userCon->block($refby);
			}


		$ref = $u[1]['level'];
		$refblock = $u[1]['block'];
		$tempblock = $u[1]['tempblock'];
		// echo $refblock;
		//return user, and user ref
		if(($ref <= $user && $refby != "1") || ($refblock == '1' && $refby != "1") || ($tempblock == '1' && $refby != "1")) return true;
		else return false;

	}

	public function upline($userRef, $userlevel){
		$levController = new LevelController();
		if($userRef != false) {
			$userModel = new User;
			//$userRef is an array that contains the user, and the user's ref,
			$refby = $userModel->getUserDetail($userRef[1]);
			//return the user, user's ref
			return $levController->callLevel($userRef[0],$refby[0]['refby'], $userlevel);
			// return array($userRef[0],$userRef[1],$refby[0]['refby']);
		}
	}

	// public function exchange($refs){
	// 	if($user != null) {
	// 		$userModel = new User;
	// 		//$userRef is an array that contains the user, and the user's ref,
	// 		$refby = $userModel->getUserDetail($userRef[1]);
	// 		//return the user, user's ref, and ref's ref
	// 		return array($userRef[0],$userRef[1],$refby[0]['refby']);
	// 	}
	// }

	// public function filterRefs($users){
	// 	$filter = array();
	// 	foreach ($users as $key => $ref) {
	// 		echo "<pre>";
	// 		//var_dump(array($ref['ref1'],$ref['ref2'],$ref['ref3'],$ref['ref4']));
	// 		// $userModel = new User;
	// 		$filter[$this->count] = array($ref['ref1'],$ref['ref2'],$ref['ref3'],$ref['ref4']);
	// 		$this->count++;
	// 	}
	// 	var_dump($filter);
	// }
}