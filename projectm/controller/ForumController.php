<?php
class ForumController{
	private $model;

	public function __construct(){
		// include_once "../bootstrap/Autoload.php";
		$this->model = new ForumModel();

	}

	public function fetchPosts($forum){
		$data = $this->model->get($forum);
		echo json_encode($data);
		// call a view
	}

	public function setPost($data){
		$post = $data[0];
		$user = $data[1];
		$forum = $data[2];
		$data = $this->model->set(array($post, $user, $forum));
		echo $data;
	}

	public function fetchComments(){

	}

	public function setComments(){

	}

	public function openThread(){

	}

	public function closeThread(){

	}
}