<?php

class AlgorithmController{
	private $userModel;
	private $refererController;
	private $userController;
	protected $id;
	private $stack = array();
	public function __construct(){
		$this->userModel = new User;
		$this->refererController = new RefererController;
		$this->userController = new UserController;
	}

	public function referer($id){
		$this->id = $id;
		//gets all the references in array value i.e ref1, ref2..., ref4
		$refs = $this->userModel->referer($this->id);
		// echo "<pre>";
		// var_dump($refs);
		return $this->check($refs, $this->id);
	}

	private function check($ref, $referer){
		//check if references are complete (4). returns a null if true
		$checkRef = $this->refererController->check($ref, $referer);
		// var_dump($checkRef);
		
		return $this->complete($checkRef);
		//exit;
	}

	private function complete($ref){
		//returns True when complete
		$complete = $this->refererController->complete($ref);
		if($complete) return $this->downlines($this->id);
		else {return $ref; }
	}

	private function downlines($refs){
		$d = $this->userController->downliners($this->userModel->referer($refs));
		//var_dump($d);
		return $this->referer($d);
		//exit;
	}

	// private function output($ref){
	// 	// echo "<pre>";
	// 	// var_dump($ref);
	// 	return $ref;
	// 	exit;
	// }
}