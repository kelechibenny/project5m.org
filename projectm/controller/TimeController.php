<?php

class TimeController{
  
  public function __construct(){
    // Set timezone
    date_default_timezone_set("Africa/Lagos");
  }

  // Time format is UNIX timestamp or
  // PHP strtotime compatible strings
  public function time($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    //echo date("Y-m-d h:i:s");
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
 
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
        break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
        // Add s if value is not 1
        if ($value != 1) {
          $interval .= "s";
        }
        // Add value and interval to times array
        $times[] = $value . " " . $interval;
        $count++;
      }
    }

    return $times;
    // Return string with times
    //return implode(", ", $times);
  }

  public function countdown($d){
    // var_dump($d);
    $gethr = explode(" ", $d[0]);
    if($gethr[1] == "second" or $gethr[1] == "seconds"){
      return "47 hours 59 minutes ".(60-@$gethr[0])." seconds";
    }
    elseif($gethr[1] == "minute" or $gethr[1] == "minutes"){
      return "47 hours ".(60-@$gethr[0])." minutes ".(60-@$d[1])." seconds";
    }
    elseif($gethr[1] == "hour" or $gethr[1] == "hours"){
      return (48-$gethr[0])."hours ".@$d[1];
    }
    elseif(($gethr[1] == "day" or $gethr[1] == "days") and $gethr[0] < 3){
      if($gethr[0] == "1") return (48-(24*$gethr[0]))."hours ".@$d[1];
      elseif($gethr[0] == "2") return false;
    }
    else{
      //return false and call user block from user controller
      return false;
    }
    
  }

  public function fourWeeks($d){
    //var_dump($d);
    $gethr = explode(" ", $d[0]);
    if($gethr[1] == "second" or $gethr[1] == "seconds"){
      return "29 days 47 hours 59 minutes";
    }
    elseif($gethr[1] == "minute" or $gethr[1] == "minutes"){
      return "29 days 47 hours ".(60-@$gethr[0])." minutes ";
    }
    elseif($gethr[1] == "hour" or $gethr[1] == "hours"){
      return "29 days ".(48-$gethr[0])." hours ".@$d[1];
    }
    elseif(($gethr[1] == "day" or $gethr[1] == "days") and $gethr[0] < 32){
      if($gethr[0] < 29) return (30 - $gethr[0])." days ";
      elseif($gethr[0] == "29") return (60 - @$d[1])." minutes ".(60 - @$d[2])." seconds";
      elseif($gethr[0] == "30") return false;
    }
    else{
      //return false and call user block from user controller
      return false;
    }
 }
 

}

?>