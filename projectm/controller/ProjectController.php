<?php

class ProjectController{
	public $promode;

	public function __construct(){
		$this->promode = new ProjectModel;
	}

	public function getProjects($cat = null, $user=null){
		$detail = $this->promode->detail($cat, $user);
		if($detail != false) return $detail;
		else return false;
	}

	public function user($user){
		// die('dfh');
		$detail = $this->promode->detail(null, $user);
		if($detail != false) return $detail;
		else return false;
	}

	public function getCategory($cat = null){
		$category = $this->promode->category($cat);
		return $category;
	}
	
	public function getAdminCategory(){
		$category = $this->promode->category_admin();
		return $category;
	}

	// public function getCategory($cat = null){
	// 	$category = $this->promode->category($cat);
	// 	return $category;
	// }

}