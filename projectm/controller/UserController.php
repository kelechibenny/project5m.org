<?php

class UserController{
	public $count = 0;
	private $stack = array();
	private $userModel;
	private $trans;
	private $refController;
	
	public function __construct(){
		$this->userModel = new User;
	}

	public function users($id = null, $level = null){
		$user = $this->userModel->getUserDetail($id, $level);
		return $user;
	}

	public function newUsers(){
		$users = $this->userModel->getUserDetail();
		foreach ($users as $key => $user) {
			extract($user); 
			$filter_date = explode(" ", $signup_date);
			// echo $filter_date[0];
			if($filter_date[0] !== date('Y-m-d')) continue;
			else $this->stack[$this->count] = $user;
			$this->count++;
		}
			return $this->stack;
	}

	public function getLevel($level){
		//Update block coloumn to one
		return $this->userModel->level($level);
	}

	public function blockedUsers(){
		//Update block coloumn to one
		return $this->userModel->getBlockedUsers();
	}

	public function userId($users){
		$filter = array();
		if($users == null){
			return 0;
		}
		else{
			foreach ($users as $key => $user) {
				// echo "<pre>";
				// var_dump($users);
				// die;
				if($user['level'] == '0' || $user['block'] == '1' || $user['id'] == '1') continue;
				$filter[$this->count] = $user['id'];
				$this->count++;
			}
			shuffle($filter);
			return $filter;
		}
	}

	public function downliners($refs){
		foreach ($refs as $key => $ref) {
			return $ref;
		}
	}

	public function block($user){
		//Update block coloumn to one
		$this->userModel->blockUser($user);
	}

	public function delete($user){
		//Update delete coloumn to one
		$this->userModel->deleteUser($user);
	}

	public function team($user){
		//create an array to hold team to 4th generation
		$this->userModel = new User;
		$ref = $this->userModel->getUserDetail($user)[0];
		
		$downline1 = array();
		$newdownline = array("0" => $ref['ref1'], "1" => $ref['ref2'], "2" => $ref['ref3'], "3" => $ref['ref4']);
		foreach($newdownline as $key => $downline){
			if($downline === '0') continue;
			$downline1[$key] = $downline;
			
		}
		
		$downline2 = array();
		if(!empty($downline1))
		foreach($downline1 as $key => $downline){
			if($downline === '0') continue;
			$this->userModel = new User;
			$ref = $this->userModel->getUserDetail($downline)[0];
			$downline2[$downline] = array($ref['ref1'],$ref['ref2'],$ref['ref3'],$ref['ref4']);
		}

		$downline33 = array();
		$downline3 = array();
		if(!empty($downline2)){
			foreach($downline2 as $key => $downline){
				array_push($downline33, $this->runArray($downline)); 
			}
			foreach($downline33 as $key => $downline){
				if(!empty($downline)){
					$downline3[key($downline)] = $downline[key($downline)];
				
					if(count($downline) > 1 ){
						foreach($downline as $key => $downlines){
							if(!empty($downlines)){
								$downline3[$key] = $downlines;
							}
						}
					}
				}
			}
		}
		
		
		$downline4 = array();
		$downline44 = array();
			if(!empty($downline3)){
			foreach($downline3 as $key => $downline){
				array_push($downline44, $this->runArray($downline)); 
			}

			foreach($downline44 as $key => $downline){
				if(!empty($downline)){
					$downline4[key($downline)] = $downline[key($downline)];
				
					if(count($downline) > 1 ){
						foreach($downline as $key => $downlines){
							if(!empty($downlines)){
								$downline4[$key] = $downlines;
							}
						}
					}
				}
			}

		}

		$team = array();
		return $team = array("1" => array($user=>$downline1), "2" => $downline2, "3" => $downline3, "4" => $downline4);
	
	}


	public function runArray($downlines){
		$narr = array();
		foreach($downlines as $key => $value){
			if($value === '0') continue;
			$this->userModel = new User;
			if($value != '0'){
				$ref = $this->userModel->getUserDetail($value)[0];
				$narr[$value] = array($ref['ref1'],$ref['ref2'],$ref['ref3'],$ref['ref4']);
			}
		}
		return $narr;

	}

	
}