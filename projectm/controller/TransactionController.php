<?php

class TransactionController{
	public $count = 0;
	private $userModel;
	private $userController;
	private $transModel;

	public function __construct(){
		$this->userModel = new User;
		$this->userController = new UserController;
		$this->transModel = new TransactionModel;
	}

	public function paidTo($user, $refs = null, $limit = null, $level =null){
		//var_dump($refs);
		$paiddetail = array();
		$paiddetail[$this->count] = $this->userModel->paidTo($user, $refs, $limit, $level);
		return $paiddetail;
	}

	public function paidBy($user, $refs = null, $limit = null, $level =null){
		// var_dump($level);
		$paiddetail = array();
		$paiddetail[$this->count] = $this->userModel->paidBy($user, $refs, $limit, $level);
		return $paiddetail;
	}

	public function transactionStatus($user, $referer, $plevel=null){
		$getStatus = $this->userModel->paidBy($user, $referer, null, $plevel);
		if($getStatus == false) return false;
		else{
			if($getStatus[0]['status'] == 0) return false;
			else return true;
		}
	}

	public function initialPaymentLevel($user){
		//die($user);
		// $this->userModel = new User;
		$limit = "LIMIT 1";
		//var_dump($user); die;
		$ref = $this->userController->users($user);
		// var_dump($ref); die;
		$referer = $ref[0]['refby'];
		$getPaymentStatus = $this->userModel->initialPaymentStatus($user, $referer, $limit);
		// var_dump($getPaymentStatus); die;

		if($getPaymentStatus == false) return false;
		else{
			if($getPaymentStatus['status'] == 0) return false;
			else return true;
		}
		//var_dump($getPaymentStatus); die;
		
	}

	public function levels(){
		$levels = array("1", "2", "3", "4", "5", "6");
		$getlevels = array();
		foreach($levels as $level){
			//get level members from last transaction. If no history, then user is just a member in level 0
			$getlevels[$this->count] = $this->userModel->getLevel($level);
			$this->count++;
		}
		return $getlevels;
	}

	public function getTotalPayment($paidby, $level){
		$t = $this->transModel->totalPayment($paidby, $level);
		return $t;
	}

	public function getTotalLevelPayment($paidby, $level){
		// $t = $this->transModel->levelPayments($paidby, $level);
		if($level == '0') $o = $this->transModel->firstLevelPayments($paidby, $level);
		else 
		{
			$t = $this->transModel->otherLevelPayments($paidby, $level);
			$o = $this->transModel->firstLevelPayments($paidby, $level);
		}
		// var_dump($level);
		switch ($level) {
			case '0':
				if($o == 1){
					//upgrade user or rather place a new clause that would not cause a block but restrict any further matching
					$this->userModel->tempBlockUser($paidby);
					return true;
				}
				else return false;
				break;

			case '1':
				if($t == 4 && $o == 1){
					$this->userModel->tempBlockUser($paidby);
					return true;
				}
				else return false;
				break;

			case '2':
				if($t == 16 && $o == 1){
					$this->userModel->tempBlockUser($paidby);
					return true;
				}
				else return false;
				break;

			case '3':
				if($t == 64 && $o == 1){
					$this->userModel->tempBlockUser($paidby);
					return true;
				}
				else return false;
				break;

			default:
				return false;
				break;
		}
		// echo '<br>';
	}


	public function putTransactionInArray($collabo,$paidby, $level){
		// echo '<br>'.($collabo);
		$t = $this->transModel->cascade($paidby, $level);
		$result = in_array($collabo, $t);
		// var_dump($collabo);
		if($result && $collabo !== '1') return true;
		else return false;
	}


	public function submitTransaction($paidby, $paidto, $amount, $plevel, $paidto_admin = null){
		// var_dump($paidto);
		
    	$db = new Database;
		$sql = $db->connect();
		$tran = $sql->query("SELECT * FROM `transactions` WHERE `paidby`= '$paidby' AND (`paidto`='$paidto' AND `status` <> '1') AND `payment_level`= '$plevel' ");
		if($tran->num_rows == '0'){
			$insert = $sql->query("INSERT INTO `transactions` (`paidby`, `paidto`, `amount`, `date`, `payment_level`, `paidto_admin` ) VALUES ('$paidby', '$paidto', '$amount', '".date('Y-m-d H:i:s')."', '$plevel', '$paidto_admin')");
			if($insert) {
		      $userdetail = new User;
		      $userdetail = $userdetail->getUserDetail($paidto);
		      $email = $userdetail[0]['email'];
		      $name = $userdetail[0]['firstname']." ".$userdetail[0]['lastname'];
		      //send email to confirming party
		      //include "bootstrap/emails/confirm_transaction.php";
		    }
				else{echo $sql->error;}
		}
		
	}
}