<?php

class LevelController{
		private $userModel;
		private $refController;
		private $transController;

	public function __construct(){
		$this->refController = new RefererController();
		$this->userModel = new User();
		$this->transController = new TransactionController();
	}

	public function callLevel($user, $refe=null, $userlevel=null){
		$level = $this->userModel->getUserDetail($user);
		// var_dump($level); die;
		extract($level[0]);
		$refby = (is_null($refe)) ? $refby : $refe;
		switch ($level){
			case 0:
					return $this->level1($user, $refby, $userlevel);
					break;
			case 1:
					return $this->level2($user, $refby, $userlevel);
					break;
			case 2:
					return $this->level3($user, $refby, $userlevel);
					break;
			case 3:
					return $this->completed($user, $refby, $userlevel);
					break;
		}

	}

	public function level1($user, $refby, $userlevel){
		//get user direct upline
		if($refby === "0") $refby = "1";
		$usr = $this->userModel->getUserDetail($refby)[1];
		$id = $usr['id'];
		// echo " ";
		$checker = $this->refController->compareLevels($user, $refby);
		$get_transaction_users = $this->transController->putTransactionInArray($id, $_SESSION['id'], $userlevel);
		// var_dump($get_transaction_users);
		// echo ' ';
		// var_dump($userlevel);
		// echo '<br>';
		if($checker == true || $get_transaction_users == true) return $this->refController->upline(array($user, $refby), $userlevel);
		else return $usr;
	}

	public function level2($user, $refby, $userlevel){
		//get user 1st level upline
		for($ref = 0; $ref < 2; $ref++){
			//var_dump($user); echo "<br>"; var_dump($refby);
			if($refby === "0") $refby = "1";
			$useRef = $refby;
			$u = $this->userModel->getUserDetail($refby);
			 extract($u[1+$ref]);
			 $u = $u[1+$ref];
		}
		// var_dump($user);
		// echo "<br>";
		// var_dump($useRef);
		// echo "<br>";
		$id = $u['id'];
		$checker = $this->refController->compareLevels($user, $useRef);
		$get_transaction_users = $this->transController->putTransactionInArray($id, $_SESSION['id'], $userlevel);
		// var_dump($get_transaction_users);
		if($checker == true || $get_transaction_users == true) return $this->refController->upline(array($user, $useRef), $userlevel);
		else return $u;
		
	}

	public function level3($user, $refby, $userlevel){
		//get user 2nd level upline
		// die('here1');
		for($ref = 0; $ref < 3; $ref++){
			if($refby === "0") $refby = "1";
			$useRef = $refby;
			$u = $this->userModel->getUserDetail($refby);
			extract($u[1+$ref]);
			$u = $u[1+$ref];
		}
		$id = $u['id'];
		$checker = $this->refController->compareLevels($user, $useRef);
		$get_transaction_users = $this->transController->putTransactionInArray($id, $_SESSION['id'], $userlevel);
		if($checker == true || $get_transaction_users == true) return $this->refController->upline(array($user, $useRef), $userlevel);
		else return $u;
	}

	public function level4($user, $refby, $userlevel){
		//get user 3rd level upline
		for($ref = 0; $ref < 4; $ref++){
			if($refby === "0") $refby = "1";
			$useRef = $refby;
			$u = $this->userModel->getUserDetail($refby);
			extract($u[1+$ref]);
			$u = $u[1+$ref];
		}
		$id = $u['id'];
		$checker = $this->refController->compareLevels($user, $useRef);
		$get_transaction_users = $this->transController->putTransactionInArray($id, $_SESSION['id'], $userlevel);
		if($checker == true || $get_transaction_users == true) return $this->refController->upline(array($user, $useRef), $userlevel);
		else return $u;
	}

	public function completed($user, $refby){
		//levels completed. 1million Naira reached!
		return "completed";
	}
}