<?php 

class AdminController extends UserController{

	public $admin;

	public function __construct(){
		// die('cameHere');
		$this->admin = new AdminModel;
	}

	public function sendMessage($detail = array()){
		$user = $detail[0];
		$sub = $detail[1];
		$msg = $detail[2];
		$response = $this->admin->sendMessage($user, $sub, $msg);
		if($response) return "Your message has been delivered to the Admin";
	}

	public function getMessages($msg = null){
		// var_dump($msg);
		return $this->admin->getMessages($msg);
	}

	public function projectCategory(){
		return $this->admin->projectCategory();
	}
}