<?php

class ImageController{

	public function setImage($image, $dir){
		$ptype= array('image/png','image/jpg','image/jpeg');
		$msg = array();
		
		if($image['size'] == null)
		{
			echo $msg[0] = '<div class="alert alert-danger">Image can\'t be empty. Please add an Image</div>';
		}
		
		else
		{
			$ftype= $image['type'];
			$name = str_replace(" ", "_", $image['name']);
			// $tname = $this->gd($image['tmp_name'],$q,$w);
			$tname = $image['tmp_name'];
			$dir = $dir.'/'.$name;
			if(!in_array($ftype, $ptype))
			{
				$msg[0] = '<div class="alert alert-danger">Image must of JPEG or PNG formats!</div>';
			}
			
		}
			if(empty($msg))
			{
				move_uploaded_file($tname, $dir);
				// imagejpeg($tname, $dir);
				return array(true, $name, null);
			}else{return array(false, null, $msg);}
	}
}