<?php 
session_start();
if(!isset($_SESSION['id']) || ($_SESSION['id'] != '1' && $_SESSION['id'] != '2' && $_SESSION['id'] != '3' && $_SESSION['id'] != '4' && $_SESSION['id'] != '5')) header("location: ../dashboard.php");
include_once "../bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "../forms.php";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Admin Dashboard</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
<span><a href="dashboard.php">User Panel</a></span>
</header>
<?php
	$userCont = new UserController;
	$users = $userCont->users();
	$users_total = count($users);				
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li>Administrator</li>
			<li><a href="admin/index.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="admin/users.php"><span class='mif-tree'></span> Manage Users</a></li>
			<li><a href="admin/project.php"><span class='mif-profile'></span> Manage Projects</a></li>
			<li><a href="admin/messages.php"><span class='mif-profile'></span> Manage Messages</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10 '>
		<?php if(isset($msg)) foreach($msg as $m) echo $m; ?>
		<h4>All users <span class='label label-info'><?php echo $users_total ?></span></h4>
		<?php if($_SESSION['id'] == '1'){ ?>
		<form id='user_action' method='POST'>
			<select name='action_value' class='col-sm-9'>
				<option value='1'>Block</option>
				<option value='2'>Unblock</option>
				<option value='3'>Mail</option>
				<?php if($_SESSION['id'] === '1'){ ?><option value='4'>Delete</option> <?php } ?>
			</select>
			<button name='user_action' class='col-sm-2'>Go</button>
		</form>
		<?php } ?>
			<table class='table table-striped'>
				<tr><th></th><th>Name</th><th>Email</th><th>Phone</th><th>Level</th><th>Last Login</th><th>Location</th></tr>
			
<?php
	foreach ($users as $detail) {
		extract($detail);
?>
		
				<tr>
					<td><input form='user_action' type="checkbox" name="check_action[]" value="<?php echo $id; ?>"></td>
					<td><?php echo ucwords($firstname." ".$lastname) ?></td>
					<td><?php echo $email; ?></td>
					<td><?php echo $phone; ?></td>
					<td><?php echo $userCont->getLevel($level)[1]; ?></td>
					<td><?php echo $lastlogin ?></td>
					<td><?php echo $location ?></td>
				</tr>
		

<?php } ?>
			</table>

<?php 
	// $userCont = new UserController;
	$newusers = $userCont->newUsers();
	// var_dump($newusers);
	$newusers_total = count($newusers);

?>
<hr>
<h4>New users <span class='label label-info'><?php echo $newusers_total ?></span></h4>
<!-- <select>
	<option value=''>Today</option>
	<option value=''>Yesterday</option>
	<option value=''>One week</option>
	<option value=''>A month ago</option>
</select> -->
			<table class='table table-striped'>
				<tr><th>Name</th><th>Email</th><th>Phone</th><th>Level</th><th>Registered</th><th>Location</th></tr>
			
<?php
	foreach ($newusers as $detail) {
		extract($detail);
?>
		
				<tr>
					<td><?php echo ucwords($firstname." ".$lastname) ?></td>
					<td><?php echo $email; ?></td>
					<td><?php echo $phone; ?></td>
					<td><?php echo $userCont->getLevel($level)[1]; ?></td>
					<td><?php echo $signup_date ?></td>
					<td><?php echo $location ?></td>
				</tr>
		

<?php } ?>
			</table>

<?php 
	// $userModel = new User;
	$blocked = $userCont->blockedUsers();
	
?>
<hr>
<h4>Blocked users <span class='label label-info'><?php echo count($blocked) ?></span></h4>
			<table class='table table-striped'>
				<tr><th>Name</th><th>Email</th><th>Phone</th><th>Level</th><th>Location</th><th>Action</th></tr>
			
<?php
if(is_array($blocked))
	foreach ($blocked as $detail) {
		extract($detail);
?>
		
				<tr>
					<td><?php echo ucwords($firstname." ".$lastname) ?></td>
					<td><?php echo $email; ?></td>
					<td><?php echo $phone; ?></td>
					<td><?php echo $userCont->getLevel($level)[1]; ?></td>
					<td><?php echo $location ?></td>
					<td>
						<?php if($_SESSION['id'] == '1'){ ?>
						<form method="get">
							<input type="hidden" name="user" value='<?php echo $id ?>'>
							<button class='btn btn-success' name='unblock' value="true">Unblock</button>
						</form>
						<?php } ?>
					</td>
				</tr>
		

<?php } ?>
			</table>
<?php 
	$adminCont = new AdminController;
	
?>
<hr>
<h4>Project Category</h4>
<?php 
	$cates = $adminCont->projectCategory();
	echo "<table><tr><td>S/N</td><td>Category</td><td>Action</td></tr>";
	if($cates !== null)
		foreach($cates as $cate) {
			extract($cate); ?>
			<tr><td><?php echo $id; ?></td><td><?php echo $category; ?></td><td><?php if($_SESSION['id'] == '1'){ ?><form><button style="max-width:100px" class='btn btn-success' value='<?php echo $id ?>' name='approve_project_category'>approve</button></form><?php } ?></td></tr>
<?php }
	echo "</table>";
?>

<hr>
<h4>Messages</h4>
<?php 
	$msgs = $adminCont->getMessages();
	echo "<table><tr><td>S/No</td><td>Type</td><td>Message</td><td>Date</td><td>Action</td></tr>";
	if($msgs !== null)
		foreach($msgs as $key => $msg) {
			extract($msg); 
			if($type == '0') $type = "Blocked Account";
			elseif($type == '1') $type = "Confirmation Delay";
			elseif($type == '2') $type = "Project Ajdustment";
			elseif($type == '3') $type = "Transaction Spamming";
			else $type = "Others";

		?>
			<tr><td><?php echo $key; ?></td><td><?php echo $type; ?></td><td><?php echo $message; ?></td><td><?php echo $date; ?></td><td><?php if($_SESSION['id'] == '1'){ ?><a href="admin/messages.php?thismsg=<?php echo $id; ?>"><button style="max-width:100px" class='btn btn-success'>View</button></a><?php } ?></td></tr>
<?php }
	echo "</table>";
?>

	</section>
</div>
	
</body>
</html>