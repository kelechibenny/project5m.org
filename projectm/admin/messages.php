<?php 
session_start();
if(!isset($_SESSION['id']) || ($_SESSION['id'] != '1' && $_SESSION['id'] != '2' && $_SESSION['id'] != '3' && $_SESSION['id'] != '4' && $_SESSION['id'] != '5')) header("location: ../dashboard.php");
include_once "../bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "../forms.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Messages</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
<span><a href="dashboard.php">User Panel</a></span>
</header>

<?php
	$userCont = new UserController;
	$users = $userCont->users();
	$users_total = count($users);				
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li>Administrator</li>
			<li><a href="admin/index.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="admin/users.php"><span class='mif-tree'></span> Manage Users</a></li>
			<li><a href="admin/project.php"><span class='mif-profile'></span> Manage Projects</a></li>
			<li><a href="admin/messages.php"><span class='mif-profile'></span> Manage Messages</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10 '>


<h4>Message Detail</h4>
<?php 
	$adminCont = new AdminController;
	$msgs = $adminCont->getMessages($_GET['thismsg']);
	echo "<table><tr><td>Type</td><td>Message</td><td>Date</td><td>Mark As</td></tr>";
	//if($msgs !== null)
		//foreach($msgs as $key => $msg) {
	// var_dump($msgs); die();
			extract($msgs[0]); 
			if($type == '0') $type = "Blocked Account";
			elseif($type == '1') $type = "Confirmation Delay";
			elseif($type == '2') $type = "Project Ajdustment";
			elseif($type == '3') $type = "Transaction Spamming";
			else $type = "Others";

		?>
			<tr><td><?php echo $type; ?></td><td><?php echo $message; ?></td><td><?php echo $date; ?></td><td><form><button style="max-width:100px" class='btn btn-success'>Resolved</button></form></td></tr>
<?php //}
	echo "</table>";
?>

<h4>User</h4>
<?php
$member = new UserController;
extract($member->users($user)[0]);
?>
<table class="table table-striped">
	<tr><td>First Name: <?php echo $firstname; ?></td></tr>
	<tr><td>Last Name: <?php echo $lastname; ?></td></tr>
	<tr><td>Email: <a href="mailto:<?php echo $email ?>"><?php echo $email; ?></a></td></tr>
	<tr><td>Phone: <?php echo $phone; ?></td></tr>
	<tr><td>Location: <?php echo @$location; ?></td></tr>
</table>

</section>
</div>
<?php
	include "../view/footer.php";
?>
</body>
</html>