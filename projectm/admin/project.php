<?php
ob_start();
session_start();
if(!isset($_SESSION['id']) || ($_SESSION['id'] != '1' && $_SESSION['id'] != '2' && $_SESSION['id'] != '3' && $_SESSION['id'] != '4' && $_SESSION['id'] != '5')) header("location: ../dashboard.php");
include_once "../bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "../forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Projects</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body id='dashboard' style="background: rgba(198,198,198,0.1)">
<header >
<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
<span><a href="dashboard.php">User Panel</a></span>
</header>
<?php
	$userModel = new User;
	$projectcate = new ProjectController;

	// $users = $userModel->getUserDetail($_SESSION['id']);
	// extract($users[0]);
?>
 <div id='dashboard'>
 <?php if(isset($_GET['update']) || isset($_GET['category'])){
 	$users = $userModel->getUserDetail($_SESSION['id']);
	extract($users[0]);
?>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li>Administrator</li>
			<li><a href="admin/index.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="admin/users.php"><span class='mif-tree'></span> Manage Users</a></li>
			<li><a href="admin/project.php"><span class='mif-profile'></span> Manage Projects</a></li>
			<li><a href="admin/messages.php"><span class='mif-profile'></span> Manage Messages</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10'>
		<div class="lone_container">
			<div class="lone_elements" >
			<span><a href="admin/project.php">&larr; Back</a></span>
			<?php
				$submit_name = "admin_add_category";
				if(isset($_GET['category'])) include "../view/add_category.php";
				// $submit_name = "admin_update_category";
				// if(isset($_GET['update'])) include "../view/update_category.php";
				
				
			?>
			</div>
		</div>
	</section>
<?php 
}else {
	// header("location: profile.php");
	$cat = (isset($_GET['cat']) && !empty($_GET['cat'])) ? $_GET['cat'] : null;
	$cates = $projectcate->getAdminCategory();
	$projects = $projectcate->getProjects($cat);
	?>
		<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li>Administrator</li>
			<li><a href="admin/index.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="admin/users.php"><span class='mif-tree'></span> Project Users</a></li>
			<li><a href="admin/project.php"><span class='mif-profile'></span> Manage Projects</a></li>
			<li><a href="admin/messages.php"><span class='mif-profile'></span> Manage Messages</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10'>
			<h3>Project Category 
			<?php if($_SESSION['id'] == '1'){ ?>
			<a href="admin/project.php?category"><span class="label label-success mif-plus" style='float: right;'>Add</span></a>
			<?php } ?>
			</h3><hr>
			<div class="well">Project5M is designed to help you raise funds for some of the most pressing projects</div>
				<form method='get' action="">
				<table class='table table-striped'>
					<tr id="<?php echo $id; ?>" ><td>Category</td><td>Status</td><td>Action</td></tr>
					<?php 
						if($cates !== null)
						foreach($cates as $cate) {
						extract($cate); ?>
						<tr id="<?php echo $id; ?>" >
							<td><?php echo $category; ?></td>
							<td>
								<?php if($status === "1"){ ?> <span class="label label-success">approved</span> <?php }else{ ?> <span class="label label-info">new</span> <?php } ?>
							</td>
							<td>
							<?php if($_SESSION['id'] == '1'){ ?>
								<?php if($status === "0"){ ?> <button class="btn btn-success" style="width:auto" value='<?php echo $id ?>' name='approve_project_category'>approve</button><?php } ?>
									&nbsp;

								<?php if($status === "0"){ ?> <button class="btn btn-success" style="width:auto" value='<?php echo $id ?>' name='decline_project_category'>decline</button><?php } ?>
									&nbsp;
								<?php } ?>

							</td>
							
						</tr>
					<?php } ?>
				</table>
				</form>
			 <br>
		
		</section>
<?php
}
 ?>
</div>
	<?php
	// include "view/footer.php";
?>
</body>
</html>
<?php ob_end_flush(); ?>