<?php 
ob_start();
session_start();
if(isset($_SESSION['id'])) header("location: dashboard.php");
date_default_timezone_set('Africa/Lagos');
include_once "bootstrap/Autoload.php"; ?>
	
<!DOCTYPE html>
<html>
<head>
	<meta name="description" content="Project5M is a platform that avails individuals an opportunity to finance their project(s) through collaboration and team work. ">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<!-- OpenGraph for Facebook. -->
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Project5M" />
	<meta property="og:url" content="www.project5m.org" />
	<meta property="og:description" content="Project5M is a platform that avails individuals an opportunity to finance their project(s) through collaboration and team work. " />
	<meta property="og:image" content="http://www.project5m.org/apps/images/p5m.png" />
	<meta property="og:locale" content="en_US" />
	<!-- Twitter Cards -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@Project5M">
	<meta name="twitter:title" content="Project5M">
	<meta name="twitter:description" content="Project5M is a platform that avails individuals an opportunity to finance their project(s) through collaboration and team work. ">
	<meta name="twitter:image:src" content="http://www.project5m.org/apps/images/p5m.png">
	
	<title>Project5M</title>
	<base href="/projectm/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<!-- <script src="js/theme.js"></script> -->
	<script src='apps/scripts/custom.js'></script>

	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/compiled/theme.css">
	<link rel="stylesheet" type="text/css" href="apps/css/vendor/animate.css">

	<!-- javascript -->
	<!-- <script src="../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
	<!-- <script src="js/bootstrap/bootstrap.min.js"></script> -->

<style type="text/css">
	.card{
		box-shadow: 0px 0px 1px gainsboro;background: #fff;cursor:pointer;
	}
</style>
	
</head>
<body id="home" class="">
	<!-- <aside class='col-xs-12 col-sm-2 col-md-2'>
		<ul>
			<li><a href="login.php">Login</a></li>
			<li><a href="register.php">Register</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-10 col-md-10'>
	Welcome

	</section> -->

	<header class="navbar navbar-inverse hero" role="banner">
  		<div class="container" >
    		<div class="navbar-header">
		      	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
		      	</button>
      			<a href="index.php" class="navbar-brand" style="padding-left:10px;padding-top:0px;" ><img src='apps/images/p5m.png' style="height:inherit;" />
	</a>
    		</div>
    		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation" >
      			<ul class="nav navbar-nav navbar-right">
      				<li id='home-slider-btn'>
        				<a href="#">
          					HOME
          				</a>
        			</li>
        			<li id="works-slider-btn">
        				<a href="#">
          					HOW IT WORKS
          				</a>
        			</li>
        			<li id="plan-slider-btn">
          				<a href="#">
          					PLAN 
          				</a>
        			</li>
        			<li id="tst-slider-btn">
        				<a href="#">
          					TESTIMONIES 
          				</a>
        			</li>
        			<li id="faq-slider-btn">
        				<a href="#">
          					FAQ 
          				</a>
        			</li>
        			<li>
        				<a href="#">
          					CONTACT
          				</a>
        			</li>
        			
      			</ul>
    		</nav>
  		</div>
	</header>

	<div id="hero">
		<div class="container">
			<h1 class="hero-text animated fadeInDown">
				Do you have a project <br />
				you need money to finance?
			</h1>
			<p class="sub-text animated fadeInDown">
				Earn 5 million Naira in 4 to 6 weeks, by collaborating with friends and family.<br/>
				Obtain a golden badge and qualify for our next BIG project!
			</p>
			<div class="cta animated fadeInDown">
				<a href="register.php" class="button-outline">REGISTER NOW!</a>
				<a href="login.php" class="button">LOGIN</a>
			</div>
			<div class="img"></div>
		</div>
	</div>

	<div id="features">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3>HOW IT WORKS</h3>
					<!-- <p>React is perfect for novice developers and experts alike.</p> -->
				</div>
			</div>
			<div class="row feature">
				<div class="col-md-6 info">
					<h4>Join a team with similar project, interest and goals. </h4>
					<p>
						You have a project you want to finance - Yes. There are several others with similar projects and drive. We believe that people of like minds achieve more together; through continuous communication and collaboration, team members can help themselves grow! 
					</p><br>
					<a href="project.php"><button class="btn btn-info" style="width:100%;height:50px;font-size:18px;">START NOW</button></a>
				</div>
				<div class="col-md-6 image">
					<img src="apps/images/team.png" class="img-responsive" alt="feature1" />
				</div>
			</div>
			<div class="divider"></div>
			<div class="row feature backwards">
				<div class="col-md-6 info">
					<h4>Collaborate and contribute finacially to grow your team and achieve individual as well as group goals.</h4>
					<p>
						Every member a team is expected to promote his/her team to ensure that everyone's vision is easily actualised. You are expected to contribute both financially up your team ladder, and recruit more members for yourself and your team members down the ladder. It is all about team work!
					</p>
					
				</div>
				<div class="col-md-6 image">
					<img src="apps/images/collaborate.png" class="img-responsive" alt="feature2" style="width:70%; height:70%;" />
				</div>
			</div>
		</div>		
	</div>

	<div id="pricing" style="background: #fff;min-height:450px">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3 style="color:#555;margin-top:5px">4 STEP PLAN</h3><br>
					<!-- <p>All plans include a 7-day free trial</p> -->
				</div>
			</div>
			<div class="col-sm-12">
				<div class="col-sm-6">
					<img src="apps/images/dash_analytics.png" style="max-width:400px;height:250px;margin-bottom: 40px;">
					<a href="register.php"><button class="btn btn-info" style="width:100%;height:50px;font-size:18px;">START NOW</button></a>
				</div>
				<div class="col-sm-6" style='font-size:18px;'>
					<div class='col-sm-12 card' style="min-height:80px;margin: 8px;"><b>Starter</b><br> Register, and contribute 5000 Naira only to get to qualify for the next round of collaboration</div>
					<div class='col-sm-12 card' style="min-height:80px;margin: 8px;"><b>Ruby</b><br> In this round, you are entitled to a total of 20,000 Naira</div>
					<div class='col-sm-12 card' style="min-height:80px;margin: 8px;"><b>Platinum</b><br> In this round, you are entitled to a total of 160,000 Naira</div>
					<div class='col-sm-12 card' style="min-height:80px;margin: 8px;"><b>Gold</b><br> This is the final round. Here you are entitled to 5,000,000 Naira with an extra reward of 120,000 Naira</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <div id="testimonials">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3>Testimonies from around the world</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="testimonial pull-right">
						<div class="quote">
							I am just quoting some stuff but I am seriously happy about this product. Has a lot of powerful
							features and is so easy to set up, I could stay customizing it day and night, it's just so much fun!
							<div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div>
						</div>
						<div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="testimonial">
						<div class="quote">
							This thing is one of those tools that everybody should be using. I really like it and with this ease to use, you can kickstart your projects and apps and just focus on your business!
							<div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div>
						</div>
						<div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- <div id="slider">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3>FREQUENTLY ASKED QUESTIONS</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 slide-wrapper">
					RHRWKERK
				</div>	
			</div>
		</div>
	</div> -->

	<div id="testimonials">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3>FREQUENTLY ASKED QUESTIONS</h3>
				</div>
			</div>
			<div class="row" style="font-size:18px;">
			<div class="col-sm-12" >
				<div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	How does the system work?</b><br>
							It's very simple. Our system is comprised of a 4x3 matrix system that helps teams support projects perculiar to them. When you join you'd need to upgrade to Ruby, then you're eligible to get support. All payments are made member to member, and upgrade is continuous until the Gold. On getting to Gold, you can decide to discontinue, or apply for a golden badge, which qualifies you for project 12Million.
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>What is a member to member payment system?</b><br>
							A member to member payment system is a system that enables members of your team pay you directly. When you get a referral, the new member will pay you directly. Basically you will receive 100% of the payment, no admin or middleman holding any funds.
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				</div>
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	Only the people at the top earn the big bucks, right?</b><br>
							WRONG! In a member forced matrix, the member who joined last, can fill his/her levels through their own advertising efforts a LOT faster than those that are simply waiting for spillover!
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>Can I skip Ruby and upgrade to Platinum or Gold?</b><br>
							No. You cannot skip levels. Our system is designed in such a way that all member must pass through every level before getting to the 5million mark. The system is built through team work and a drive to achieve purpose, therefore each level is an opportunity to network and inspire one another.
<!-- Please note: If you upgrade to the next level before your direct sponsor, your payment will go to the wrong person, meaning your upline misses out on income, please contact your referrer if he/she has NOT upgraded yet to the same level and give them a chance to do so! You can check the current levels of your upline on the Upgrade Account page. -->

							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				</div>
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	How do I make my payment?</b><br>
							You can make you payment through direct bank channels using the account details provided by your Collaborator. Other channels of payment are also accepted if they're convenient for you and your collaborator. 
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>Do I have to build a team in order to earn?</b><br>
							No. You don't have to build your team (you however need a team), as the system makes it possible for people to automatically join you (aka spillover) when they register, or manually when they are interested in your project. However, by relying on spillovers only, it may take a long time to build up your team and start getting support.
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				</div>
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	Can I Refer More Than 4 members?</b><br>
							Yes! You could invite any number of new members and they will be placed in your team.
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>Can I have more than one account?</b><br>
							It is up to you. More account may mean more financial committment for you, and may slow down the process. Its up to you reall, however you cannot have more than one account with similar details; the system is designed to checkmate that. 
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				</div>
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>	Why has my account been deleted?</b><br>
							This could be because are ready to upgrade to the next level but have not in the last 48hours; You may have been reported for spamming, invalid/multiple transaction Id for several accounts, or your account was temporary blocked due to inactivity.
Kindly contact admin if you think there has been misunderstanding or error!

							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>Do you give refunds?</b><br>
							Your membership is NON-REFUNDABLE. We believe you were interested in and committed to your sponsors project before joining his team, and since your money is paid directly by your to members of your team, we are not liable to refund you. 
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>I have paid into my upline account but he denied receiving payment and refuse to confirm my payment.</b><br>
							It is advisable to pay into your upline account so that on issues like this you will be ask to upload your details of payment so that the admin can verify such claims. If we find him/she guilty, his/her account will be blocked without notification.
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				<div class="col-sm-6">
					<div class="">
						<div>
						<b>I submitted my payment but no one has confirmed yet, what should I do?</b><br>
							Please allow up to 24 hours for your team sponsor to approve your payment, or try to contact them immediately. If for some reason days pass, please contact admin immediately. Also, we have an auto approve system just in case admin is not reachable. It will take 48 hours after you have submitted your transaction details for the system to approve you automatically. But we are always around, this shouldn't be a problem. 
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div> -->
					</div>
				</div>
				</div>
				<!-- <div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	I have paid, but have not been confirmed yet. What do I do?</b><br>
							Please try to communicate with the member you sent the payment to. It is reasonable to allow at least 24 hours for them to confirm you in the system because of different time zones. If you are unable to communicate with the member; please contact us, and we will help resolve the issue for you.
							<div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div>
						</div>
						<div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div>
					</div>
				</div> -->
				<!-- <div class="col-sm-6">
					<div class="">
						<div>
						<b>Do we have to pay taxes on monies received?</b><br>
							Please consult your tax professional for laws in your country. 
							<div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div>
						</div>
						<div class="author">
							<img src="apps/images/testimonials/testimonial2.jpg" class="pic" alt="testimonial2" />
							<div class="name">Karen Jones</div>
							<div class="company">Pixar Co.</div>
						</div>
					</div>
				</div> -->
				<div class="col-sm-12" style="margin-top:25px">
				<div class="col-sm-6">
					<div class=" ">
						<div>
						<b>	Is my personal information protected with your company?</b><br>
							Any personal information that you provide to us is protected. Your information will never be shared or sold to anyone!
							<!-- <div class="arrow-down">
				                <div class="arrow"></div>
				                <div class="arrow-border"></div>
				            </div> -->
						</div>
						<!-- <div class="author">
							<img src="apps/images/testimonials/testimonial3.jpg" class="pic" alt="testimonial3" />
							<div class="name">John McClane</div>
							<div class="company">Microsoft</div>
						</div> -->
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>


	<div id="footer-white">
		<div class="container">
			<div class="row credits">
				<div class="col-md-12">
					Copyright © <?php echo date('Y'); ?>. PROJECT5M
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>
<?php ob_end_flush(); ?>