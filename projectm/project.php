<?php
// die('projects');
ob_start();
session_start();
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Projects</title>
	<meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body id='dashboard' style="background: rgba(198,198,198,0.1)">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
	 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if(isset($_SESSION['id']) && ($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5')){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$projectcate = new ProjectController;

	// $users = $userModel->getUserDetail($_SESSION['id']);
	// extract($users[0]);
?>
 <div id='dashboard'>
 <?php if(isset($_GET['add']) || isset($_GET['category'])){
 	$users = $userModel->getUserDetail($_SESSION['id']);
	extract($users[0]);
?>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10'>
		<div class="lone_container">
			<div class="lone_elements" >
			<?php
				$submit_name = "add_category";
				if(isset($_GET['add'])) include "view/add_project.php";
				elseif(isset($_GET['category'])) include "view/add_category.php";
				
			?>
			</div>
		</div>
	</section>
<?php 
}else {
	// header("location: profile.php");
	$cat = (isset($_GET['cat']) && !empty($_GET['cat'])) ? $_GET['cat'] : null;
	$cates = $projectcate->getCategory();
	$projects = $projectcate->getProjects($cat);
	?>
		<div class='col-xs-12'>
			<p class='col-xs-12'>
				<h3>Project Scout <span style="font-size: 10px"><a href="index.php">Home</a> / projects</span>
				</h3><hr>
				<div class="well">Project5M is designed to help you raise funds for some of the most pressing projects. With <b>Project Scout</b>, you can find teams with projects that interest you!</div>
				<form method='get' action="">
				<span class="label label-info" style="float: left;padding:10px;margin:6px;"><a href="project.php">all</a></span>
				<select name='cat' class='col-xs-7 col-sm-7'>
					<option disabled selected>Choose a project category</option>
						
					<?php 
						if($cates !== null)
						foreach($cates as $cate) {
						extract($cate); ?>
						<option value="<?php echo $id; ?>" ><?php echo $category; ?></option>
					<?php } ?>
				</select>
				<input type="submit" name="p_c" value="Go" class='col-xs-2' />
				</form>
			</p>  <br>
		<?php if($projects != false) {?>
			<p style="clear:both"><span class='label label-success'>Total <?php echo count(($projects)); ?></span></p>
			<?php foreach ($projects as $project) {
				extract($project); 
			?>
			<div class='col-xs-5 col-sm-3 card' style="margin:20px;word-wrap: break-word;">
				<h4><?php echo $title; ?></h4>
				<p><?php echo $description; ?></p>
				<p>Like this project? <a href="<?php echo "register.php"."?r=".$email; ?>" target="_blank"><span class='label label-success'>Join team</span></a></p>
			</div>
			<?php } } ?>
		</div>
<?php
}
 ?>
</div>
	<?php
	//include "view/footer.php";
?>
</body>
</html>
<?php ob_end_flush(); ?>