<?php
date_default_timezone_set('Africa/Lagos');
include_once "bootstrap/Autoload.php";
/********************Registration*****************/
$msg = array();
if (isset($_POST['registration'])){
$db = new Database;
$sql = $db->connect();
		  
		  
  if(empty($_POST['firstname'])){$msg[0] ='<div class="alert alert-danger"> Your firstname please</div>';}
  elseif(!filter_var($_POST['firstname'], FILTER_SANITIZE_STRING)){$msg[0] = '<div class="alert-danger"> Only alphabets please</div>';}
  else{$firstname= $_POST['firstname'];}

  if(empty($_POST['lastname'])){$msg[0] ='<div class="alert-danger"> Your lastname please</div>';}
  elseif(!filter_var($_POST['lastname'], FILTER_SANITIZE_STRING)){$msg[0] = '<div class="alert-danger"> Only alphabets please</div>';}
  else{$lastname= $_POST['lastname'];}

  if(empty($_POST['phone'])){$msg[0] ='<div class="alert-danger"> Your phone number please</div>';}
  elseif(!filter_var($_POST['phone'], FILTER_SANITIZE_STRING)){$msg[0] = '<div class="alert-danger"> Only numbers please</div>';}
  else{$phone= $_POST['phone'];}

  if(!empty($_POST['email'])){
    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){$msg[1]='<div class="alert-danger"> Invalid email format</div>';}
    else{$email = $_POST['email'];}
  }else{$msg[1] = '<div class="alert-danger"> Email is required. Please insert an Email</div>'; }
    
  if(empty($_POST['password'])){$msg[3] ='<div class="alert-danger"> Please input a password in the field</div>';}
  else{$password= sha1($_POST['password']);}

  if(empty($_POST['rpassword'])){$msg[4] ='<div class="alert-danger"> Please re-type your password in the field</div>';}
  elseif($_POST['rpassword']!= $_POST['password']){$msg[4] ='<div class="alert-danger"> Password mismatch</div>';}
  else{$rpassword= sha1($_POST['rpassword']);}

  $transactionController = new TransactionController;
  if(isset($_POST['ref']) and !empty($_POST['ref']))
  {
    $getRefID= $sql->query("SELECT `id` FROM `users` WHERE `email`='".$sql->real_escape_string($_POST['ref'])."' OR `id`='".$_POST['ref']."' ");
    if($getRefID->num_rows == 1){
      echo $p_ref = $getRefID->fetch_assoc()['id'];
      $pay_level = $transactionController->initialPaymentLevel($p_ref);
      if($pay_level) $ref = $p_ref; 
      else $ref = NULL;
    }
    else $ref = NULL;
  }
  else $ref = NULL;
// exit;
  // var_dump($ref);

  $dp = 'apps/images/icon0.png';

  if(isset($phone) && isset($email) && isset($firstname) && isset($lastname) && isset($rpassword)){
		$checkdb= $sql->query("SELECT * FROM `users` WHERE `phone`='$phone' OR `email`='$email' ");
		  if($checkdb->num_rows == 0) {
		    
		    $UserController = new UserController;
		    $algo = new AlgorithmController;
		  if(!empty($ref)){
		    	$d = $algo->referer($ref);
          // echo "1";
				//$updateRef = $sql->query("UPDATE `users` SET {$d[0]} = {$memberId} WHERE `id`= {$d[1]}");
			}
			elseif(empty($ref)){
				//get all users
				$us = $UserController->users(null, '1');
				//get all user ids and return a random array of user IDs
				$d = $UserController->userId($us);
				// var_dump($d); //die;
				if($d == '0') $d = array('0', '0');
				else $d = $algo->referer($d[0]); //then pass any first id into algo->referer();
        // echo "2";
			}
      // echo "<br>";
      // var_dump($d); 
      // die;
			if(isset($d)) {
        $key = md5(uniqid(mt_rand(),true));
				$insert= $sql->query("INSERT INTO `users` (`phone`, `firstname`, `lastname`,`password`, `email`, `refby`, `signup_date`, `level_date`, `block`, `Akey`, `dp` ) VALUES ('$phone', '$firstname', '$lastname','$password', '$email', '{$d[1]}', '".date('Y-m-d h:i:s')."', '".date('Y-m-d h:i:s')."', '1', '{$key}', '$dp' )");
			    $memberId = $sql->insert_id;
			    
			    if($d[1] != '0') $updateRef = $sql->query("UPDATE `users` SET {$d[0]} = {$memberId} WHERE `id`= {$d[1]}");
			}
			// else die("check for again. Something is wrong");
		    if($insert)
		    {
		      $msg[0] = '<br/><div class="alert-success"> Registered</div>';
          $name = $firstname." ".$lastname;
          /*****Send an Email******/
          include 'bootstrap/emails/email_verify.php';

		    }
		    //else{echo $sql->error;}
		  }
		  else
		  {
		    $msg[0] = '<br/><div class="alert-danger"> Ooops! Email/ Phone number already exists!!</div>';
		  }
	}
}

/*********Log In/Sign In Portal Script*********/
  if (isset($_POST['sign_in'])) {
    //include_once 'c:/xampp/htdocs/2015/quick.ng/classes/database.php';
    $select= new Database();
    $mysql = $select->connect();

    if(empty($_POST['email']) && empty($_POST['password'])){
      $msg[0] = '<div class="alert-danger">Ooops! Display Name/Email and Password fields can\'t be empty!</div>';
    }
    elseif(empty($_POST['email'])){
      $msg[0] = '<div class="alert-danger">Display Name/Email field is empty!</div>';
    }
    elseif(empty($_POST['password'])){
      $msg[0] = '<div class="alert-danger">Password field is empty!</div>';
    }
    
    if(empty($msg)){
      $email = $_POST['email'];
      $password = sha1($_POST['password']);
    }
    if(isset($email) && isset($password)){
      // echo "1";
      $checkdb= $mysql->query("SELECT * FROM `users` WHERE `email`='$email' AND `password`='$password' ");
      if($checkdb->num_rows == 1){
        $user = $checkdb->fetch_array();
        $userid = $user['id'];
        $mysql->query("UPDATE `users` SET `login_date` = '".date('Y-m-d h:i:s')."', `lastlogin` = '".$user['login_date']."' WHERE `id`= $userid ");
        
        $_SESSION['id'] = $userid;          
  			header('Location: dashboard.php');
  		}
       else{$msg[1] = '<div class="alert-danger">This account does not exist</div>';}
    }
     else{$msg[1] = '<div class="alert-danger">A valid Email and Password is required</div>';} 
        
}



if(isset($_POST['submit_transaction'])){
    $db = new Database;
	$sql = $db->connect();
		// $select_level = $sql->query("SELECT `level` FROM `users` WHERE `id` = '{$user}' ");
    $userdetail = new User;
    $user = $_POST['user'];
    $userdetail = $userdetail->getUserDetail($user);
    $level = $userdetail[0]['level'] + 1;
    $update_user_level = $sql->query("UPDATE `users` SET  `level`='{$level}', `tempblock` = '0', `level_date` = '".date('Y-m-d h:i:s')."' WHERE `id` = '{$user}' ");
    
		if($update_user_level) {
      $email = $userdetail[0]['email'];
      $name = $userdetail[0]['firstname']." ".$userdetail[0]['lastname'];
      //send email to confirming party
      include "bootstrap/emails/upgrade_complete_email.php";
      $msg[0] = "<div class='alert alert-success'>You have been upgraded</div>";
    }
}


if(isset($_POST['confirm_transaction'])){
	if(empty($_POST['paidby'])){
	  	$msg[5] ='<div class="alert-danger"> Confirmation declined!</div>';
	  	sleep(5);
	  	header("location: ".$_SERVER['SERVER_NAME']);
	  }
	  else{$paidby = $_POST['paidby'];}

  if(empty($_POST['paidto'])){
  	$msg[6] ='<div class="alert-danger"> Confirmation declined!</div>';
  	sleep(5);
  	header("location: ".$_SERVER['SERVER_NAME']);
  }
  else{$paidto = $_POST['paidto'];}

  if(empty($_POST['tid'])){
  	$msg[6] ='<div class="alert-danger"> Confirmation declined!</div>';
  	sleep(5);
  	header("location: ".$_SERVER['SERVER_NAME']);
  }
  else{$tid = $_POST['tid'];}

	if(isset($paidto) && isset($paidby))
	{
		$db = new Database;
		$sql = $db->connect();
    // $update_trans = $sql->query("UPDATE `transactions` SET  `status`='1', `payment_level`='1' WHERE `id` = '{$tid}' ");
		$update_trans = $sql->query("UPDATE `transactions` SET  `status`='1' WHERE `id` = '{$tid}' ");
    if($update_trans) {
      $delete_others = $sql->query("DELETE FROM `transactions` WHERE `paidto` = '{$paidto}' AND `paidby` = '{$paidby}' AND `payment_level` = '$plevel' AND `status` = '0' ");
      $get_paid_to_wallet = $sql->query("SELECT `wallet` FROM `users` WHERE `id` = '{$paidto}' ");
      $get_amount = $sql->query("SELECT `amount` FROM `transactions` WHERE `id` = '{$tid}' ");
      $wallet = $get_paid_to_wallet->fetch_assoc()['wallet'];
      $amount = $get_amount->fetch_assoc()['amount'];
      $newwallet = $wallet + $amount;
      $update_paid_to_wallet = $sql->query("UPDATE `users` SET  `wallet`='{$newwallet}' WHERE `id` = '{$paidto}' ");
      // echo $msg[0] = '<div class="alert-success"> The transaction was successfully confirmed</div>';
      $userdetail = new User;
      $userdetail = $userdetail->getUserDetail($paidby);
      $email = $userdetail[0]['email'];
      $name = $userdetail[0]['firstname']." ".$userdetail[0]['lastname'];
      //send email to confirming party
      include "bootstrap/emails/transaction_confirmation.php";
      
    }
	}
}

if(isset($_POST['add_account'])){
// var_dump($_POST['accountname']);
  if(empty($_POST['accountname'])){$msg[0] ='<div class="alert-danger"> Your exact account name is required</div>';}
  else{$accountname = $_POST['accountname'];}

  if(empty($_POST['bank'])){$msg[1] ='<div class="alert-danger"> The name of your bank is required</div>';}
  else{$bank = $_POST['bank'];}

  if(empty($_POST['accountnumber'])){$msg[2] ='<div class="alert-danger"> Your account number is required</div>';}
  else{$accountnumber = $_POST['accountnumber'];} 

  if(empty($_POST['userid'])){
    $msg[6] ='<div class="alert-danger"> Confirmation declined!</div>';
    sleep(5);
    header("location: ".$_SERVER['SERVER_NAME']);
  }
  else{$user = $_POST['userid'];}

  if(isset($accountname) && isset($bank) && isset($accountnumber))
  {
    $db = new Database;
    $sql = $db->connect();
    $update = $sql->query("UPDATE `users` SET `accountname`='{$accountname}', `accountnumber`='{$accountnumber}', `bank`= '{$bank}' WHERE `id`='{$user}' ");
    if($update) {
      $msg[0] = '<div class="alert-success"><span class="mif-info mif-2x"></span> Account infomation saved</div>';
      // sleep(5);
      header("location: profile.php");
    }
    else{$msg[4] = '<div class="alert-danger">Action unsuccessful. Try again!</div>';}
  }
  // else $msg[4] = '<div class="alert-danger">All fields are required!</div>';

}


if(isset($_POST['add_project'])){
  // die('here');
// var_dump($_POST['accountname']);
  if(empty($_POST['category'])){$msg[0] ='<div class="alert-danger"> The category of your project is required</div>';}
  else{$category = $_POST['category'];}

  if(empty($_POST['title'])){$msg[1] ='<div class="alert-danger"> The title of your project is required</div>';}
  else{$title = $_POST['title'];}

  if(empty($_POST['description'])){$msg[2] ='<div class="alert-danger"> Give your project a concise description</div>';}
  else{$description = $_POST['description'];} 

  if(empty($_POST['userid'])){
    $msg[6] ='<div class="alert-danger"> Project declined!</div>';
    sleep(5);
    header("location: ".$_SERVER['SERVER_NAME']);
  }
  else{$user = $_POST['userid'];}

  if(isset($category) && isset($title) && isset($description))
  {
    // die('here');
    $db = new Database;
    $sql = $db->connect();
    $check = $sql->query("SELECT `user` FROM `projects` WHERE `user` = '{$user}' ");
    if($check->num_rows == '0'){
      $insert = $sql->query("INSERT INTO `projects` (`category`, `title`, `description`, `user`) VALUES ('{$category}', '{$title}', '{$description}', '{$user}') ");
      if($insert) {
        $msg[0] = '<div class="alert-success"><span class="mif-info mif-2x"></span> Project infomation saved</div>';
        // sleep(5);
        header("location: profile.php");
      }
      else{$msg[4] = '<div class="alert-danger">Action unsuccessful. Try again!</div>';}
    }
    else $msg[1] = '<div class="alert-danger">Action unsuccessful. You already have a project. To make any adjustments, kindly contact the administrator.</div>';
  }
  // else $msg[4] = '<div class="alert-danger">All fields are required!</div>';

}


if(isset($_POST['add_category'])){
  $db = new Database;
  $sql = $db->connect();
  if(isset($_POST['category'])) $category = $sql->real_escape_string($_POST['category']);
  $insert = $sql->query("INSERT INTO `projects_category` (`category`, `status`) VALUES ('{$category}', '{1}') ");
  if($insert) {
      $msg[0] = '<div class="alert-success"><span class="mif-info mif-2x"></span> Category added to queue. Awaiting approval</div>';
      header("location: ".$_SERVER['PHP_SELF']."?add");
  }
  //else echo $sql->error;

}


if(isset($_POST['admin_add_category'])){
  $db = new Database;
  $sql = $db->connect();
  if(isset($_POST['category'])) $category = $sql->real_escape_string($_POST['category']);
  $select = $sql->query("SELECT `category` FROM `projects_category` WHERE `category` = '$category' ");
  if($select->num_rows > 0){
    $msg[0] = '<div class="alert-danger"><span class="mif-cross mif-2x"></span> Category already exists!</div>';
  }
  else{
    $insert = $sql->query("INSERT INTO `projects_category` (`category`, `status`) VALUES ('{$category}', '1') ");
    if($insert) {
        $msg[0] = '<div class="alert-success"><span class="mif-info mif-2x"></span> Category Successfully Added</div>';
        // header("location: ".$_SERVER['PHP_SELF']."?add");
    }
    //else echo $sql->error;
  }

}


//Update profile information
if(isset($_POST['save_dp'])){
  // if($_FILES['dp']['size'] > 0) $msg[0] = "<p class='alert-danger'>Your dp is required!</p>"; else $dp = $_POST['dp'];
  $db = new Database;
  $imge = new ImageController;
  $sql = $db->connect();
  $dir = 'bootstrap/uploads/profiles/';
  list($img, $name, $msg) = $imge->setImage($_FILES['dp'], $dir);
  $dp = 'bootstrap/uploads/profiles/'.$name;
  $user = $_POST['userid'];
  if(empty($msg) and $img == 1){
    $updateRef = $sql->query("UPDATE `users` SET `dp` = '{$dp}' WHERE `id`= '{$user}' ");
    if($updateRef) $msg[0] = "<p class='alert-success'>Your dp has been updated</p>";
  }
  // else echo $sql->error;
}

if(isset($_POST['save_fn'])){
  if(empty($_POST['fn'])) $msg[0] = "<p class='alert-danger'>Your first name is required!</p>"; else $fn = $_POST['fn'];
  $user = $_POST['userid'];
  $db = new Database;
  $sql = $db->connect();
  if(!isset($msg)){
    $updateRef = $sql->query("UPDATE `users` SET `firstname` = '{$fn}' WHERE `id`= '{$user}' ");
    if($updateRef) $msg[0] = "<p class='alert-success'>Your first name has been updated</p>";
  }
}

if(isset($_POST['save_ln'])){
  if(empty($_POST['ln'])) $msg[0] = "<p class='alert-danger'>Your last name is required!</p>"; else $ln = $_POST['ln'];
  $user = $_POST['userid'];
  $db = new Database;
  $sql = $db->connect();
  if(!isset($msg)){
    $updateRef = $sql->query("UPDATE `users` SET `lastname` = '{$ln}' WHERE `id`= '{$user}' ");
    if($updateRef) $msg[0] = "<p class='alert-success'>Your last name has been updated</p>";
  }
}

if(isset($_POST['save_phone'])){
  if(empty($_POST['phone'])) $msg[0] = "<p class='alert-danger'>Your phone is required!</p>"; else $phone = $_POST['phone'];
  $user = $_POST['userid'];
  $db = new Database;
  $sql = $db->connect();

  if(!isset($msg)){
    $checkdb= $sql->query("SELECT * FROM `users` WHERE `phone`='$phone'");
    if($checkdb->num_rows == 0) {
      $updateRef = $sql->query("UPDATE `users` SET `phone` = '{$phone}' WHERE `id`= '{$user}' ");
      if($updateRef) $msg[0] = "<p class='alert-success'>Your phone number has been updated</p>";
    }
  }
}

if(isset($_POST['save_location'])){
  if(empty($_POST['location'])) $msg[0] = "<p class='alert-danger'>Your location column is empty. Please add a location to proceed!</p>"; else $location = $_POST['location'];
  $user = $_POST['userid'];
  $db = new Database;
  $sql = $db->connect();
  if(!isset($msg)){
    $updateRef = $sql->query("UPDATE `users` SET `location` = '{$location}' WHERE `id`= '{$user}' ");
    if($updateRef) $msg[0] = "<p class='alert-success'>Your location has been saved</p>";
  }
}


//change password
if(isset($_POST['password_change'])){

  if(empty($_POST['pass1'])){$msg[3] ='<div class="alert-danger">You didn\'t place anything in the password field</div>';}
  else{$pass1= sha1($_POST['pass1']);}

  if(empty($_POST['pass2'])){$msg[4] ='<div class="alert-danger"> Kindly re-type your password for confirmation</div>';}
  elseif($_POST['pass2']!= $_POST['pass1']){$msg[4] ='<div class="alert-danger">Both passwords didn\'t match</div>';}
  else{$pass2= sha1($_POST['pass2']);}

  $user = $_POST['userid'];
  $email = $_POST['email'];

  if(isset($pass2)){
  //die('came here 1');
    $db = new Database;
    $sql = $db->connect();
    $updateRef = $sql->query("UPDATE `users` SET `password` = '{$pass2}' WHERE `id`= '{$user}' AND `email`= '{email}' ");
    if($updateRef) $msg[0] = "<p class='alert-success'>Your new password has been saved</p>";
    if(isset($_POST['set'])){
    	//die('came here');
      setcookie('verify', 'Your new password has been saved. Login to proceed to your dashboard', time()+120);
      header('location: login.php');
    }
  }
  else{
  //echo $sql->error;
  }

}


if(isset($_POST['contact_admin'])){
  if(!isset($_POST['message']) || !isset($_POST['email']) || !isset($_POST['subject']))
  {
    $msg[0] = 'Your email was not sent. Every field is required!';
  }
  //send mail to admin.
  $email = $_POST['email'];
  $subject = $_POST['subject'];
  $message = $_POST['message'];
  if(mail($email, $subject, $message)){
    //send message to admin
    $get_admin = new AdminController;
    $get_admin->sendMessage(array($user, $subject, $message));
    // if($message) $msg[0] = 'Your message has been sent to the Admin!';

  }
}


if(isset($_GET['unblock'])){
  $user = $_GET['user'];
  $db = new Database;
  $sql = $db->connect();
  $userdetail = new User;
  $userdetail = $userdetail->getUserDetail($user)[0];
  // var_dump($userdetail);
  extract($userdetail);
  $updateRef = $sql->query("UPDATE `users` SET `block` = '0', `level_date` = '".date('Y-m-d h:i:s')."'
                              WHERE `id`= '{$user}' ");
  if($updateRef){
    //send message to user
    // mail($email, $subject, $message)
    $name = $firstname." ".$lastname;
    include "bootstrap/emails/unblock_email.php";

  }
}


if(isset($_POST['user_action'])){
  $db = new Database;
  $sql = $db->connect();
   
  if(!empty($_POST['check_action'])){
  
    foreach ($_POST['check_action'] as $key => $user) {
      if($_POST['action_value'] == '1')
      $action = $sql->query("UPDATE `users` SET `block` = '1' WHERE `id`= '{$user}' ");

      elseif($_POST['action_value'] == '2')
      $action = $sql->query("UPDATE `users` SET `block` = '0',  `level_date` = '".date('Y-m-d h:i:s')."' WHERE `id`= '{$user}' ");

      elseif($_POST['action_value'] == '4')
      {
         $userModel = new User;
        $userModel->deleteUser($user);
      }
    }
    
    if(isset($action)){
      $msg[0] = "<p class='alert alert-success'>Action applied</p>";

    }
    //else $msg[0] = $sql->error;
  }
  else $msg[0] = "<p class='alert alert-info'>You must select a user to perform this action. Check a box to continue</p>";
}

if(isset($_GET['approve_project_category'])){
  $db = new Database;
  $sql = $db->connect();
  $id = $_GET['approve_project_category'];

  $action = $sql->query("UPDATE `projects_category` SET `status` = '1' WHERE `id`= '{$id}' ");
  if($action){
    $msg[0] = "<p class='alert alert-success'>Project Category approved</p>";
  }
}

if(isset($_REQUEST['getCountdown'])){
  $controller = new TimeController;
  $d = $controller->time($_REQUEST['cdate'], $_REQUEST['rdate']);
  echo $countdown = $controller->countdown($d);
}


if (isset($_POST['forgotpassword'])) {
  $db = new Database;
  $sql = $db->connect();

  if(empty($_POST['email'])){
    $msg[0] = "<div class='alert alert-danger'> Email field is empty!</div><br/>";
  }
  else{
    $email = $_POST['email'];
    $checkuser = $sql->query("SELECT `id`, `email` FROM `users` WHERE `email`='$email' ");
    if($checkuser->num_rows==1) {
      $g = $checkuser->fetch_assoc();
      $id = $g['id'];
      $key = md5(uniqid(mt_rand(),true));
      //echo $id;
      //echo "<br>";
      //echo $key;
      //die;
    $insertuser= $sql->query("UPDATE `users` SET `Akey` = '".$key."' WHERE `id` = '".$id."' " );
      if($checkuser){
        include 'bootstrap/emails/forgot_pass_email.php';
      }
    } 
    else  $msg[0] = "<div class='alert-danger'> This Email does not exist OR you don't have an account yet</div><br/>";
  }
}