<?php 
session_start();
if(!isset($_SESSION['id'])) header("location: index.php");
date_default_timezone_set('Africa/Lagos');
include_once "bootstrap/Autoload.php";
include "forms.php";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	
	<meta charset='utf-8'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
    
	<base href="/projectm/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a>
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$refererController = new RefererController;
	$userController = new UserController;
	$bankModel = new BankModel;

	$users = $userModel->getUserDetail($_SESSION['id']);
	// var_dump($users[0]); die;
	extract($users[0]);
	$myemail = $email;
				
?>
<div id='dashboard'>

	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10 '>
	<?php
	if($block == 0){
		if(!isset($_GET['confirm']) && !isset($_GET['requestconfirm'])) { ?>
		<!--div>
			<button class="btn btn-success">Wallet Deposits</button>
			<button class="btn btn-success">Wallet Withdrawals</button>
		</div--><br>

		<?php 
			$userlevel = $level;
			// echo $refby;
			// $getTransactionStatus = $user->transactionStatus($_SESSION['id'], $refby,$userlevel);
			// var_dump($getTransactionStatus);
			// $topay = ($getTransactionStatus == false) ? "To pay" : "Paid";
			$topay = "Pay";
			$userReff = $refby;
		?>
		<h3>My Dashboard</h3>
		<div class='col-sm-7 col-xs-12 col-md-7'>
			<h4>Profile</h4>
			<table class='table table-striped col-sm-12'>
				<tr>
				<td>Current level</td>
				<td>
				<?php 
				$userlevel = $level;
				if($level < 3){
					$paymentfee = $userModel->level($level)[2];
					echo $userModel->level($level)[1]." <span class='label label-info'>".$topay." &#8358;".number_format($paymentfee)." to qualify for next level</span>";
				}
				else{
					echo "<span class='label label-info'>Gold</span>";
				}
				?>
				</td>
				</tr>
				<tr>
				<td>Next level</td>
				<td>
				<?php 
				if($level < 3){
					echo $userModel->level($level+1)[1]." <span class='label label-success'>To receive &#8358;".number_format($userModel->level($level+1)[3])."</span>"; 
				}
				else{
					echo "<span class='label label-info'>Become a P5M Alumni</span>";
				}
				$mybank = $bank;
				?>
					
				</td>
				</tr>
				<tr><td>Joined</td><td><?php echo $signup_date; ?></td></tr>
				<tr><td>Last login</td><td><?php echo $lastlogin; ?></td></tr>
				<tr><td>Wallet</td><td>&#8358;<?php echo number_format($wallet); ?></td></tr>
			</table>

			<div class="col-sm-12" style="font-size: 16px;margin: 5px; padding:20px; min-height: 60px;box-shadow: 0px 0px 1px gainsboro">
			<span class="mif-key"></span> Referral ID<br><b>http://www.project5m.org/register.php?r=<?php echo $email ?></b>
			</div>
		</div>
		<?php 
	// var_dump($users[0]); die;

			//get sign up date before parsing to the next object
			$registered = $level_date;
			//$upline = $userController->uplines(0);
			//print_r($upline);

		?>


		<div class='col-sm-5 col-xs-12 col-md-5'>
			<?php

				$user = new TransactionController;
				
				//check payments to admin
				$paidto_admin = ($refby == 1 && $_SESSION['id'] !== '1') ? $refby : null;

				$getStats = $user->getTotalPayment($_SESSION['id'], $userlevel);
				$updrade_ = $user->getTotalLevelPayment($_SESSION['id'], $userlevel);
				var_dump($updrade_);
				// echo "<br>";
				// echo $getStats;
				// echo "<br>";
				$levController = new LevelController;
				$levCon = $levController->callLevel($_SESSION['id'], null, $userlevel);
				// echo $levCon['id'];
				if(($getStats == '0') && $levCon !== "completed"){
					extract($levCon);
					$submitTrans = $user->submitTransaction($_SESSION['id'], $id, $paymentfee, $userlevel, $paidto_admin);
				}
					
				// var_dump($levCon);
				if($levCon !== "completed"){
					// extract($levCon);
					//submit user detail to be paid by this user to transactions table 
					
					$limit = "LIMIT 1";
					$user = new TransactionController;
					$paidbqy = $user->paidTo($_SESSION['id'], null, $limit, $userlevel);
					// var_dump($paidbqy);
					extract($paidbqy[0][0]);

					$name = $firstname." ".$lastname;
					$phone = $phone;
					$email = $email;
					//echo $level;
				$time = new TimeController;
				$d = $time->time(date("Y-m-d h:i:s"), $registered);
				// echo $userlevel;
				if($userlevel == 0){
					// echo "djz";
					// if($updrade_){
						$countdown = $time->countdown($d);
						if($countdown == false && $_SESSION['id'] != '1') $userController->block($_SESSION['id']);
						$count_tag = "You have ".$countdown." to upgrade or be deleted";
					// }
					// else $count_tag = "Get ready. Your next payment is on the way!";
				}
				elseif($userlevel == 1){
					// echo "dj";
					if($updrade_){
						$countdown = $time->fourWeeks($d);
						if($countdown == false && $_SESSION['id'] != '1') $userController->block($_SESSION['id']);
						$count_tag = "You have ".$countdown." to complete your team and upgrade";
					}
					else $count_tag = "Get ready. Your next payment is on the way!";
				}
				elseif($userlevel > 1){
					// echo "djz";
					if($updrade_){
						$countdown = $time->countdown($d);
						if($countdown == false && $_SESSION['id'] != '1') $userController->block($_SESSION['id']);
						$count_tag = "You have ".$countdown." to upgrade";
					}
					else $count_tag = "Get ready. Your next payment is on the way!";
					
				}
				// $paymentEnds = $time->paymenttimeline($d);

				$user = new TransactionController;
				$getTransactionStatus = $user->transactionStatus($_SESSION['id'], $id, $userlevel);
				// var_dump($userlevel);
				
			?>
			<div class='well alert-info'><h4>Pay this collaborator <?php echo '&#8358;' . number_format($paymentfee); ?></h4>
			<?php if($userlevel != '0'){ ?>
				Ensure you have received enough money up to the amount payable on this level before you pay your collaborator and Upgrade
			<?php } ?>
			</div>
			<?php
			// var_dump($getStats);
			if($getStats == '0'){ ?>
			<table class='table table-striped'>
				<tr><td>Email</td><td><?php echo $email; ?></td></tr>
				<tr><td>Phone</td><td><?php echo $phone; ?></td></tr>
				<tr><td>Account Name</td><td><?php echo @$accountname; ?></td></tr>
				<tr><td>Account No.</td><td><?php echo @$accountnumber; ?></td></tr>
				<tr><td>Bank</td><td><?php echo @$bankModel->getBank($bank)[0]['bank']; ?></td></tr>
				<tr><td>Level</td><td><?php echo $level; ?></td></tr>
			</table>
			<?php }
				else{ ?>
				<div style="text-align:center;">
					<a href='dashboard.php?requestconfirm&payto=<?php echo $id; ?>&plevel=<?php echo $userlevel; ?>&email=<?php echo $email; ?>&pay=<?php echo $paymentfee; ?>'><button class='btn btn-success' style='width:190px;height:150px'>Upgrade to <?php echo $userModel->level($userlevel+1)[1];?></button></a>
				</div>
				<?php } 
					?>
						<span class="label label-info"><?php echo $count_tag; ?></span>
			

		<?php 
			}
			//
			else{ ?>
			<div style="height: 300px; box-shadow: 1px 1px 0px 2px #f9f9f9">
				<img src="apps/images/goldenbadge.png" style="max-width: 150px; max-height: 150px; margin: 20px 120px;">
				<div style="text-align:center;font-size: 18px;"><b>Congratulation! You are at the final lap of Project5million</b></div>
				<div style="text-align:center"><span class="btn btn-success">What Next?</span></div>
			</div>
		<?php } 
		//}
		?>
		</div>


		<div class='col-sm-12' style="margin-bottom: 50px;clear:both">

			<?php if(empty($mybank)){ ?>
			<div class='col-xs-5 card' style="margin: 10px; min-height: 70px; padding: 20px; color:red; "><a href='account.php?add'><span class="mif-warning mif-3x"></span><b>Update Account Information</b></a></div>
			<?php } ?>

			<?php $proCon = new ProjectController;
			if($proCon->user($_SESSION['id'])[0] == false){ ?>
			<div class='col-xs-5 card' style="margin: 10px; min-height: 70px; padding: 20px; color:red; "><a href='project.php?add'><span class="mif-warning mif-3x"></span><b>Update Project Information</b></a></div>
			<?php } ?>
		</div>

		<div style='clear:both'>
		<?php
			$users = $userModel->getUserDetail($_SESSION['id']);
			//var_dump($users);
		extract($users[1]);
		?>
			<h4>Referral Summary</h4>
			<?php 
			$paidto = $user->paidTo($_SESSION['id'], null, null, null);
			// var_dump(count($paidto[0]));
			if($paidto[0] == false){
			 ?>
			<div class='alert alert-info'>Make sure you pay your upline before you refer people. If your referral registers before you pay your upline, the system will automatically assign him to someone who has made an upline payment instead</div>
			<?php } 
	
	// //gets all the references in array value i.e ref1, ref2..., ref4
	$refs = $userModel->referer($id);

	//check if references are complete (4). returns a null if true
	$checkRef = $refererController->check($refs);

	//gets the 
	$refDetails = $refererController->detail($refs);
	// var_dump($refDetails);
	
	if(!empty($refDetails[0])){
		?>
			<table class='table table-striped'>
				<tr><!-- <th>ID</th> --><th>Name</th><th>Level</th><!-- <th>Payment Stat</th> --><th>Wallet</th></tr>
			
<?php
		foreach ($refDetails as $detail) {
			if($detail === null) continue;
			extract($detail[0]);
			// if($payout == 0) $payout = "<span class='label label-danger'>Not Paid</span>";
			// elseif($payout == 1) $payout = "<span class='label label-success'>Paid</span>";
	
	?>
			
					<tr>
						<!-- <td><?php //echo $id ?></td> -->
						<td><?php echo $firstname." ".$lastname ?></td>
						<td><?php echo $userModel->level($level)[1]; ?></td>
						<!-- <td><?php //echo $payout ?></td>-->
						<td><?php echo "&#8358;".$wallet ?></td>
					</tr>
			

	<?php } ?>
		</table>
<?php }else{
			echo "<p style='text-align:center;font-size:16px;'><i>You have not referred anyone yet!</i></p>";
		} ?>
			
		</div><hr><br>

		<?php
			// $user = new TransactionController;
			// $limit = "LIMIT 5 ";

			

			echo "<div>
			<h4>Top Transactions <span style='font-size:10px;color:blue'><a href='transactions.php'> show all</a></span></h4>
			<h5 class='label label-info'>Paid By</h5>";
			echo "<table class='table table-striped'><tr><th>Name</th><th>Phone</th><th>Level</th><th>Amount Paid</th><th>Status</th><th>Action</th></tr>";
			$pay_levels = array("0" => "4", "1" => "16", "2" => "64", "3" => "128");
			$count = 0;
			// var_dump($pay_levels);
			foreach ($pay_levels as $key => $value) {
				$limit = "LIMIT ".$value."";
				$paidby = $user->paidBy($_SESSION['id'], null, $limit, $key);
				$lm = new LevelModel;
				$level = $lm->getLevel($key)[0]['level'];
				if($level == null) $level = "Starter";
				if($paidby[0] == false) continue;
				echo "<tr><th colspan='6' style=''><span>".$level."</span></th></tr>";
				foreach($paidby[0] as $paid){ 
					if($paid === false) continue;
					extract($paid);
					// var_dump($paidby);
					$stat = ($status == 0) ? "<span class='label label-danger'>Unconfirmed</span>" : "<span class='label label-success'>Confirmed</span>";
					// if($level == null) $level = "Member";
				?>
					<tr>
						<td><?php echo $firstname." ".$lastname ?></td>
						<td><?php echo $phone ?></td>
						<td><?php echo $level ?></td>
						<td><?php echo $amount ?></td>
						<td><?php echo $stat ?></td>
						<td>
							<?php if($status == 0){
								$userModel = new User;
								$getreflevel = $userModel->getUserDetail($paidby);
								@extract($getreflevel[0]);
							?>
							<a href='dashboard.php?confirm&paidby=<?php echo urlencode($firstname)." ".urlencode($lastname); ?>&uid=<?php echo $paidby; ?>&tid=<?php echo $paid[0]; ?>&pl=<?php echo $level; ?>' ><button class='btn btn-success'>Confirm</button></a>
							<?php } ?>
						</td>
					</tr>
				<?php
				// if($count == $value)
				$count++;
				 }
				}
				echo "</table>";
			echo "</div><hr>";
			}
			elseif(isset($_GET['requestconfirm'])) include("view/submit_transaction_form.php");
			elseif(isset($_GET['confirm'])) include("view/confirm_transaction.php");

		}
		else echo "<div>
			<h3 class='alert alert-info'><span class='mif-lock'></span> Your Account is currently Locked</h3>
			
			<div class='col-sm-12'>
				<h4>New User?</h4>
				<p>Activate Account. Check your email account for a verification mail and follow the instructions</p><br>
			</div>
			<div class='col-sm-6 '>
				<h4>Blocked?</h4>
				<h5 class='alert-danger'>Implications of a blocked account:</h5>
				<ol>
					<li>Your account will remain blocked for 7days (penalty week) after contacting Admin</li>
					<li>You will not receive any contributions during this period</li>
					<li>You will not have access to your dashboard during this period</li>
					<li>This may ultimately cause a delay in your progress</li>
				</ol>
			</div>
			<div class='col-sm-6'>
				<h4>Contact Admin</h4>
				<p class='alert-info'>If you have a very cogent reason, your account may be unblocked before your penalty week is complete</p>
				<form method='post'>
					<input type='text' value='Blocked Account' readonly />
					<input type='hidden' value='1' name='subject' />
					<input type='hidden' name='email' value='".$myemail."' />
					<textarea placeholder='Message' name='message' required></textarea>
					<button name='contact_admin'>Submit</button>
				</form>
			</div>
			</div>";
		?>

	</section>
	
</div>
<!-- <footer class="col-md-12">
Copyright © <?php echo date('Y'); ?>. PROJECT5MILLION
</footer> -->
<?php
	//include "view/footer.php";
?>
</body>
</html>