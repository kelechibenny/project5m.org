<?php
// die('projects');
ob_start();
session_start();
include_once "../bootstrap/Autoload.php";
include "../forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Docs</title>
	
	<meta charset='utf-8'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body id='dashboard' style="background: rgba(198,198,198,0.1)">
<header >
	<img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" />
	<span class='mif-menu mif-3x' id='nav-menu'></span>
</header>
 <div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><a href="doc/?terms-and-conditions"><span></span> Terms and Conditions</a></li>
			<li><a href="doc/?frequently-asked-questions"><span></span> FAQ</a></li>
			<li><a href="#"><span></span> Privacy</a></li>
			<li><a href="#"><span></span> About</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10'>
		<?php 
		if(empty($_GET) || isset($_GET['terms-and-conditions']))
		{ 
			echo file_get_contents('../view/terms.html');
		}
		elseif(isset($_GET['frequently-asked-questions']))
		{ 
			echo file_get_contents('../view/faq.html');
		}
		elseif(isset($_GET['privacy-concerns'])){
			echo file_get_contents('../view/privacy.html');
		}
		elseif(isset($_GET['learn-more'])){
			echo file_get_contents('../view/about.html');
		}  ?>
	</section>

</div>
	<?php
	//include "../view/footer.php";
?>
</body>
</html>
<?php ob_end_flush(); ?>