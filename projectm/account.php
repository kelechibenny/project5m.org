<?php
ob_start();
session_start();
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Account</title>
	<meta charset='utf-8'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
    
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body id='dashboard' style="background: rgba(198,198,198,0.1)">
<?php if(isset($_GET['add']) || isset($_GET['update'])){?>
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a> 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$bankModel = new BankModel;

	$users = $userModel->getUserDetail($_SESSION['id']);
	extract($users[0]);
?>
 <div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10' style="">
		<div class="lone_container">
			<div class="lone_elements" style=''>
			<?php
				if(isset($_GET['add'])) include "view/add_account.php";
				elseif(isset($_GET['update'])) include "view/update_account.php";
				
			?>
			</div>
		</div>
	</section>
</div>
<?php } 
else {
	header('location: profile.php'); die;
}
 ?>
 <?php
	//include "view/footer.php";
?>
</body>
</html>
<?php ob_end_flush(); ?>