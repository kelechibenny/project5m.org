<?php 
session_start();
include "forms.php";
if(isset($_GET['r'])){ 
	//setcookie('refid', $_GET['r'], time()+604800);
	setcookie('ref', $_GET['r'], time()+604800);
	// echo $_GET['refid'];
	header('location: index.php?set&ref=true&trigger_js');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/projectm/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body style="background: rgba(0,0,245,0.1)">
<!-- <header >logo</header>-->
<div class='col-md-12' id='regbox'>
<div  style='padding: 20px; /*border: 1px solid #f0f0f0;*/ min-height: 350px;box-shadow: 0px 0px 5px 0px #e7e7e7;background: #fff'>
<a href="/"><img src="apps/images/p5m.jpg" style="width:50%;height:50%;margin-left: 25%;" ></a> <hr>
<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>'>
	<table>
		<tr><td><h3><span class="mif-event-available"></span>Register</h3></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td>
			<input id='firstname' style='' name='firstname' type='text' value='<?php if(isset($_POST['firstname'])){echo $_POST['firstname'];} ?>' placeholder='First Name (Required)' required/>
		</td>
		</tr>
		<tr>
		<td>
			<input id='lastname' style='' name='lastname' type='text' value='<?php if(isset($_POST['lastname'])){echo $_POST['lastname'];} ?>' placeholder='Last Name (Required)' required/>
		</td>
		</tr>
		<tr>
		<td><input required name='email' type='email' value='<?php if(isset($_POST['email'])){echo $_POST['email'];} ?>' placeholder='Email Address (Required)' title='Ensure you give a valid email address ' /></td>
		</tr>
		<tr>
		<td><input id='phone' name='phone' type='text' pattern='[0-9]{11}' value='<?php if(isset($_POST['phone'])){echo $_POST['phone'];} ?>' placeholder='Phone Number (Required)' title='A valid phone number of 11 characters' required/></td>
		</tr>
		<tr>
		<td>
			<div style="margin:5px;width:100%"><input class="col-sm-6" style="margin:0px" name='password' type='password' value='<?php if(isset($_POST['password'])){echo $_POST['password'];} ?>' placeholder='Password' title='You are advised to use a password of 8 or more character combinations' required />

			<input class="col-sm-6" style="margin:0px" name='rpassword' type='password' value='<?php if(isset($_POST['rpassword'])){echo $_POST['rpassword'];} ?>' placeholder='Re-type Password' required /></div>
		</td>
		</tr>
		<tr>
			<td>
			<input name='ref' type='text' value='<?php if(isset($_COOKIE['ref'])){ echo $_COOKIE['ref']; } elseif(isset($_GET['r'])){echo $_GET['r'];} elseif(isset($_POST['ref'])){echo $_POST['ref'];} ?>' placeholder='Referer (if any, use refererID)' />
			</td>
		</tr>
		<tr>
		<td>
			
		</td>
		</tr>
		<tr>
		<td><br/>
		<p style="color:gray">&nbsp;By clicking on "Register", you agree to our <a href="docs/" target="_blank" style="color:blue">Terms and Policies</a></p>
		<input type='submit' name='registration' style='background-color: #0080C0; color:#fff' value='Register' /></td>
		</tr>
		<tr>
		<td><span>Already a member? <a href='login.php' style='color:#0080C0'>Login Now</a></span></td>
		</tr>
	</table>
</form>
</div>
</div>
</body>
</html>