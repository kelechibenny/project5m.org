<?php 
spl_autoload_register('Autoload::norm');
spl_autoload_register('Autoload::onestep');
spl_autoload_register('Autoload::twostep');

class Autoload{

	public static function norm($class){
		$root = '/';
		//$classes = 'classes/';
		// $includes = 'includes/';
		$model = 'model/';
		$controller = 'controller/';
		$ext = '.php';
		if(file_exists($class.$ext))
			include_once $class.$ext;
		elseif(file_exists($root.$class.$ext))
			include_once $root.$class.$ext;
		elseif(file_exists($model.$class.$ext))
			include_once $model.$class.$ext;
		elseif(file_exists($controller.$class.$ext))
			include_once $controller.$class.$ext;
		// elseif(file_exists($models.$class.$ext))
		// 	include_once $models.$class.$ext;
	}

	public static function onestep($class){
		$root = '../';
		//$classes = 'classes/';
		// $includes = 'includes/';
		$model = '../model/';
		$controller = '../controller/';
		$ext = '.php';
		if(file_exists($class.$ext))
			include_once $class.$ext;
		elseif(file_exists($root.$class.$ext))
			include_once $root.$class.$ext;
		elseif(file_exists($model.$class.$ext))
			include_once $model.$class.$ext;
		elseif(file_exists($controller.$class.$ext))
			include_once $controller.$class.$ext;
		// elseif(file_exists($models.$class.$ext))
		// 	include_once $models.$class.$ext;
	}

	public static function twostep($class){
		$root = '../../';
		//$classes = 'classes/';
		// $includes = 'includes/';
		$model = '../../model/';
		$controller = '../../controller/';
		$ext = '.php';
		if(file_exists($class.$ext))
			include_once $class.$ext;
		elseif(file_exists($root.$class.$ext))
			include_once $root.$class.$ext;
		elseif(file_exists($model.$class.$ext))
			include_once $model.$class.$ext;
		elseif(file_exists($controller.$class.$ext))
			include_once $controller.$class.$ext;
		// elseif(file_exists($models.$class.$ext))
		// 	include_once $models.$class.$ext;
	}

	
}
	