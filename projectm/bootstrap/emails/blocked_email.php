<?php
ob_start();
$subject = "Blocked Account";
$message = "
<html>
<body style='background: #EBEAF5; padding: 20px 0px;'>
<div style='border:1px solid silver; padding: 0px 20px 100px; width:65%; background:white; margin:20px auto;'>
<a href='http://www.project5m.org'><img src='www.project5m.org/apps/images/p5m.jpg' style='width:50%;height:50%;margin-left: 20%;' ></a>
<div style='color:#000'>
<p>Hello $name</p>
<p>Your Project5Million Account has been blocked! This is because your upgrade timeline exceeded 48 hours.</p>
<p>The implications of a blocked account are as follows</p>
<ul>
	<li>Your account will remain blocked for 7days (penalty week) after submitting a complain to Admin</li>
	<li>You will not receive any contributions during this period</li>
	<li>You will not have access to your dashboard during this period</li>
	<li>This may ultimately cause a delay in your progress</li>
</ul>
<p>This will however not cause any challenge for your team members both uplines and downlines, who will keep getting up the ladder unitl they accomplish their goals</p><br>
<p>To resolve this problem immediately, Log into your www.project5m.org account and send a message to the Admin about this. If you have a very cogent reason, your account may be unblocked before your penalty week is complete</p><br>
<p>Warmest regards</p>
<p>Your Project5million Team</p>
</div>
</div>

</body>
</html>
";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=ISO-8859-1" . "\r\n"; 
$headers .= "From: Project5million <admin@project5m.org>" . "\r\n" . "Reply-To: Project5million <admin@project5m.org>";
@mail($email, $subject, $message, $headers);
ob_end_flush();                  

?>