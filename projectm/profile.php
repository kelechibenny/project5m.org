<?php 
session_start();
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<meta charset='utf-8'>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body id='dashboard' style="background: rgba(198,198,198,0.1)">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a> 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$refererController = new RefererController;
	$userController = new UserController;

	$bankModel = new BankModel;

	$users = $userModel->getUserDetail($_SESSION['id']);
	extract($users[0]);
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10' >
	<h3>&nbsp;&nbsp;&nbsp;&nbsp;My Profile</h3>
	
		<div class='col-sm-12'>
			<div class="col-md-6" style="min-height: 300px;box-shadow: 1px 1px 1px  gainsboro;background: #fff;padding: 20px;margin:10px;font-size: 20px">
				<div id='profile_div'>
				<?php if(isset($msg)) foreach($msg as $m) echo "<tr><td>".$m."</td></tr>"; ?>
				<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="userid" value="<?php echo $_SESSION['id']; ?>">
					<table>
						<tr><th><span class='mif-user mif-2x'></span> Personal</th><th></th></tr>
						<tr><td><img src="<?php echo $dp; ?>"></td><td><input style="display:none" id="dp" type="file" name="dp"></td><td><span class="mif-pencil" id="edit_dp"></span></td></tr>
						<tr><td>Email</td><td><input pattern="[a-zA-Z0-9@.]+" title="A valid email address only" disabled id="email" type="email" name="email" value="<?php echo $email; ?>"></td><td></td></tr>
						<tr><td>First Name</td><td><input disabled id="fn" type="text" name="fn" value="<?php echo $firstname; ?>"></td><td><span class="mif-pencil" id="edit_fn"></span></td></tr>
						<tr><td>Last Name</td><td><input disabled id="ln" type="text" name="ln" value="<?php echo $lastname; ?>"></td><td><span class="mif-pencil" id="edit_ln"></span></td></tr>
						<tr><td>Phone Number</td><td><input pattern="[0-9]{11}" title="Phone number is an eleven digit number" disabled id="phone" type="text"  name="phone" value="<?php echo $phone; ?>"></td><td><span class="mif-pencil" id="edit_phone"></span></td></tr>
						<tr><td>Location</td><td><input disabled id="location" type="text" name="location" value="<?php echo @$location; ?>"></td><td><span class="mif-pencil" id="edit_location"></span></td></tr>
					</table><br>
					</form>
				</div>
			</div>
			<div class="col-md-5 col-sm-12" style="margin:10px;font-size: 20px">
			<div class='col-sm-12 card' style="padding: 20px;min-height: 180px;">
				<?php if(!empty($bank)){ ?>
				<table>
					<tr><th>Account</th><th><a href='account.php?update'><span class="mif-pencil mif-2x" style="float: right; color:#f0f0f0"></span></a></th></tr>
					<tr><td>Name of Account</td><td><?php echo $accountname; ?></td></tr>
					<tr><td>Bank</td><td><?php echo $bankModel->getBank($bank)[0]['bank']; ?></td></tr>
					<tr><td>Account Number</td><td><?php echo $accountnumber; ?></td></tr>
				</table>
				<?php }else{ ?>
					<div class='mif-cabinet' style='font-size:90px;color:gainsboro;padding:10%;'></div>
					<p class='flex' style='color:gainsboro;'>No account detail. Add an account so your collaborators can transact easily with you.</p>
					<div style="margin-top:50px;"><a href='account.php?add'><button class="btn btn-success">Add an Account</button></a></div>
				<?php } ?>
				</div>

				<div class="col-sm-12 card" style="min-height: 250px;margin-top:10px;">
				<h3>Change Password</h3>
				<form action="" method="post">
					<input type="hidden" name="userid" value='<?php echo $_SESSION['id']; ?>'>
					<input type="hidden" name="email" value='<?php echo $email; ?>'>
						
					<table>
						<tr><td><input type="password" name="pass1" placeholder="password"></td></tr>
						<tr><td><input type="password" name="pass2" placeholder="confirm password"></td></tr>
						<tr><td><button name='password_change' style='background-color: #0080C0; color:#fff'>Change password</button></td></tr>
					</table>
				</form>
			</div>
			
			</div>
		</div>

		<div class='col-sm-12'>
			<div class="col-md-6 card" style="min-height: 200px;padding: 20px;margin:5px 10px;font-size: 20px">
				<?php 
						$proCon = new ProjectController;

						if($projects = $proCon->user($_SESSION['id'])){
							extract($projects[0]);
							// var_dump($projects[0]);
					?>
					<table>
						<tr><th><span class='mif-stack mif-2x'></span> Project</th><th></th></tr>
						<tr><td>Category</td><td><?php echo $category; ?></td></td></tr>
						<tr><td>Misson</td><td><?php echo $title; ?></td></td></tr>
						<tr><td>Description</td><td><?php echo $description; ?></td></tr>
					</table>
					<?php 
						}
						else {
							echo "<div class='mif-suitcase' style='font-size:90px;color:gainsboro;padding:10%;'></div>
					<p class='flex' style='color:gainsboro;'>You do not have a project yet. Create a project, join a project community, and build your team.</p>
					<div style='margin-top:50px;'><a href='project.php?add'><button class='btn btn-success'>Add a Project</button></a></div>";
						}
					?>
			</div>
		</div>

	</section>
</div>
	<?php
	//include "view/footer.php";
?>
</body>
</html>