$(document).ready(function(){

	function postComments(post, user, forum){
		$.ajax({
			type: "POST",
			url: "forum/forum.php?setPost&post="+post+"&user="+user+"&forum="+forum,
			cache: false,
			success: function(data) {getPosts(forum);}
		});
	}

	function getPosts(forum){
		$.ajax({
			type: "GET",
			url: "forum/forum.php?getPosts&forum="+forum,
			cache: false,
			success: function(data) {
				// console.log(JSON.parse(data));
				var obj = JSON.parse(data);
				var html = "";
				var i;
				for(i = 0; i < obj.length; i++) {
			        html += '<div class="card" style="width:100%;min-height:100px;border:1px solid gainsboro;margin:0px;padding:15px;background:#fff"> \
			        <p><img style="width:30px;height:30px" src="' + obj[i].dp + '" /> \
			        <span>' + obj[i].firstname + ' ' + obj[i].lastname + '</span><br> \
			        <span style="font-size:7px;">' + obj[i].date + '</span></p> \
			        <p>' + obj[i].post + '</p> \
			        <p style="font-size:9px;color:green" id="'+ obj[i].id +'"><span>Comment</span></p> \
			        </div> \
			        <br>';
			    }
			    document.getElementById("show_threads").innerHTML = html;
				
			}
		});
	}

	$(document).on('click', '#forum_post_submit', function(event){
		// console.log('clicked');
		var forumid = $('#forumid').val();
		var userid = $('#userid').val();
		var forum_post = $('#forum_post').val();
		var forum_posts = escape(forum_post);
		if(forum_post !== ""){
			$(this).html("Loading..");
			postComments(forum_posts, userid, forumid);
		}
		
	});

	var patt = /http:\/\/localhost\/projectm\/forum\/\?ffid=[0-9]+/;
	var string = location.href;
	var vallu = patt.exec(string);
	console.log(string);
	if (vallu != null){
		valu = vallu[0].split("=");
		if(location.href == vallu[0]){
			getPosts(valu[1]);
		}
	}

});