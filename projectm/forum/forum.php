<?php
include_once "../bootstrap/Autoload.php";

if(isset($_REQUEST['setPost'])){
	$post = $_REQUEST['post'];
	$user = $_REQUEST['user'];
	$forum = $_REQUEST['forum'];

	//send to controller
	$controller = new ForumController;
	$controller->setPost(array($post, $user, $forum));
}

if(isset($_REQUEST['getPosts'])){
	$forum = $_REQUEST['forum'];

	//send to controller
	$controller = new ForumController;
	$controller->fetchPosts($forum);
}