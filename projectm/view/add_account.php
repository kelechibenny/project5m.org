<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF'])."?add";?>'>
	<table>
		<tr><td><h3>Add Account</h3></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td>
			<input id='accountname' name='accountname' type='text' value='<?php if(isset($_POST['accountname'])){echo $_POST['accountname'];} ?>' placeholder='Name of your account (Required)' required />
		</td>
		</tr>
		<tr>
		<td>
			<select id='bank' name='bank' required>
				<?php 
					// $bankModel = new BankModel;
					$banks = $bankModel->getBank();
					echo "<option disabled >Select a bank</option>";
					foreach ($banks as $bank) {
						extract($bank);
						echo "<option value='".$id."'>".$bank."</option>";
					}
				?>
			</select>
			<input type="hidden" name="userid" value="<?php echo $_SESSION['id']; ?>">
		</td>
		</tr>
		<tr>
		<td><input required name='accountnumber' type='text' pattern="[0-9]{10}" title="Your account number is a 10 digit number" max='10' value='<?php if(isset($_POST['accountnumber'])){echo $_POST['accountnumber'];} ?>'  placeholder='Your bank account number (Required)'  /></td>
		</tr>
		<tr>
		<td><br/><input type='submit' name='add_account' style='background-color: #0080C0; color:#fff' value='Add Account' /></td>
		</tr>
	</table>
</form>