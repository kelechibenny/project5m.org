<?php 
if(!isset($_SESSION['id'])) header('location: /');
if(!isset($_GET['payto']) || !isset($_GET['plevel']) || !isset($_GET['pay']))
	header('location: dashboard.php');
// include "forms.php";
//create array();
$userModel = new User;
$users = $userModel->getUserDetail($_SESSION['id']);
// var_dump($users);
extract($users[0]);
				
?>

<h3>Upgrade Area</h3><hr>
				
<?php if(isset($msg)) foreach($msg as $m) echo $m; ?>
	<div class="lone_container">
		<div class="lone_elements" style=''>
			<h4 class='well'>Before you continue with the upgrade, please ensure that the following is correct.</h4>

			<div class='alert-info'>
				<ul>
					<li><b>Starter</b>: You have paid your grand collaborator #5,000</li>
					<li><b>Ruby</b>: You have received a total of #20,000</li>
					<li><b>Platinum</b>: You have received a total of #160,000</li>
					<li><b>Gold</b>: You have received a total of #5,120,000</li>
				</ul>
			</div>
			<form action="" method="post" class="form-horizontal">
				<div style="padding:20px;">
					<input type="hidden" name="user" value="<?php echo $_SESSION['id']; ?>">
					<input type="submit" name="submit_transaction" value="continue to upgrade" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>