<?php
//confirm transaction
//the transaction table
// include "forms.php";
// if(isset($msg)) foreach($msg as $m) echo $m; 
$userModel = new User;
			//gets all the references in array value i.e ref1, ref2..., ref4
			// $refs = $userModel->referer(1);
			$user = new TransactionController;
			$limit = "LIMIT 1";
			$paidy = $user->paidBy($_SESSION['id'], $_GET['uid'], $limit, $_GET['pl']);
			// var_dump($paidy);

			if($paidy[0] != false){
echo "<div>
			<h3>Confirmation for ".$_GET['paidby']."</h3>
			<p class='alert-info'>Make sure you have received the correct amount of money</p>
			<table class='table table-striped'>";
			
			foreach($paidy[0] as $paid){ 
				if($paid === false) continue;
				extract($paid);
				$bankModel = new BankModel;
				// var_dump($paidy[0][0][8]);
				$bankby = $bankModel->getBank($paidy[0][0][8])[0]['bank'];
				// var_dump($paid);
				$status = ($status == 0) ? "Unconfirmed" : "Confirmed";
				if($level == null) $level = "Starter";
			?>
				<tr><th>Name</th><td><?php echo $firstname." ".$lastname ?></td></tr>
				<tr><th>Level</th><td><?php echo $level ?></td></tr>
				<tr><th>Amount Paid</th><td><?php echo $amount ?></td></tr>
				<tr><th>Account Paid From</th><td><?php echo $account ?></td></tr>
				<tr><th>Reference Number</th><td><?php echo $reference ?></td></tr><tr><th>Paid on</th><td><?php echo $date ?></td></tr>
				<tr><th>Bank paid from</th><td><?php echo $bankby ?></td></tr>
				<tr><th>Paid on</th><td><?php echo $date ?></td></tr>
				<tr><td>
					<form action="" method='post'>
						<input type="hidden" name="paidby" value="<?php echo $_GET['uid']; ?>" >
						<input type="hidden" name="paidto" value="<?php echo $_SESSION['id']; ?>" >
						<input type="hidden" name="tid" value="<?php echo $_GET['tid']; ?>" >
						<input type="hidden" name="plevel" value="<?php echo $payment_level; ?>" >
						
						<button class='btn btn-success' name="confirm_transaction">Confirm</button>
					</form>
				</td><td></td></tr>
			<?php } 
			echo "</table></div>";
		}
		else {
			echo "<div class='mif-paper-plane' style='font-size:150px;color:gainsboro;padding: 20% 40%;'></div>
			<p class='flex' style='color:gainsboro;padding: 0% 36%;'>Transaction confirmation sent already.</p>";
		}