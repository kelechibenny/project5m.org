<p class="alert alert-danger"><span class="mif-warning"></span> Your project can only be added once. Kindly check and recheck to make sure you're on track before pushing the 'Add Project' button </p>
<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF'])."?add";?>'>
	<table>
		<tr><td><h3>Add Project</h3></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td>
			<!-- <input list='category_' name='category' type='text'  placeholder='Choose a Project category (Required)' required /> -->
			<select id='category_' name='category' required >
				<?php 
					// $bankModel = new BankModel;
					$projects = $projectcate->getCategory();
					foreach ($projects as $project) {
						extract($project);
						echo "<option value='".$id."'>".$category."</option>";
					}
				?>
			</select>
		</td>
		</tr>
		<tr>
		<td>
			<input type="text" name="title" placeholder='Give your project a compelling mission statement (Required)' maxlength='30' title="Short compelling title. Maximum of 30 characters" required >
			<input type="hidden" name="userid"  value="<?php echo $_SESSION['id'] ?>" required >
		</td>
		</tr>
		<tr>
		<td><textarea placeholder="Describe your project vision (Required)" name='description' id="add_project" maxlength="105" ></textarea><br>
		<span id="add_project_span" style="color:red;float:right">105</span>max: 105</td>
		</tr>
		<tr>
		<td><br/><input type='submit' name='add_project' style='background-color: #0080C0; color:#fff' value='Add Project' /></td>
	</table>
</form>
<br/><a href='project.php?category'><button>Create a new category</button></a>