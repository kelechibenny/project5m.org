<h3>Add category</h3>
<p class='alert alert-info'><span class='mif-info mif-3x'></span> Check to make sure that this category doesn't already exist. </p>
<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF'])."?category";?>'>
	<table>
		<tr><td></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td>
			<input type="text" name="category" placeholder='Add a new category (Required)' required >
		</td>
		</tr>
		<tr>
		<td><br/><input type='submit' name='<?php echo $submit_name ?>' style='background-color: #0080C0; color:#fff' value='Save new category' /></td>
	</table>
</form>