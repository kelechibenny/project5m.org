<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF'])."?update";?>'>
	<table>
		<tr><td><h3>Update Account</h3></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td>
			<input id='accountname' name='accountname' type='text' value='<?php if(isset($_POST['accountname'])){echo $_POST['accountname'];}else{echo $accountname;} ?>' placeholder='Name of your account (Required)' required />
		</td>
		</tr>
		<tr>
		<td>
			<select id='bank' name='bank' required>
				<?php 
					$mybank = $bankModel->getBank($bank)[0]['id'];
					$banks = $bankModel->getBank();
					echo "<option selected disabled >Select a bank</option>";
					foreach ($banks as $bank) {
						extract($bank);
						$s = ($mybank === $id) ? 'selected' : null;
						echo "<option value='".$id."' {$s} >".$bank."</option>";
					}
				?>
			</select>
			<input type="hidden" name="userid" value="<?php echo $_SESSION['id']; ?>">
		</td>
		</tr>
		<tr>
		<td><input required name='accountnumber' type='text' pattern="[0-9]{10}" title="Your account number is a 10 digit number" max='10' value='<?php if(isset($_POST['accountnumber'])){echo $_POST['accountnumber'];}else{echo $accountnumber;} ?>'  placeholder='Your bank account number (Required)'  /></td>
		</tr>
		<tr>
		<td><br/><input type='submit' name='add_account' style='background-color: #0080C0; color:#fff' value='Update Account' /></td>
		</tr>
	</table>
</form>