<?php 
session_start();
include_once "bootstrap/Autoload.php"; 
date_default_timezone_set('Africa/Lagos');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Transactions</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body class="">
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a> 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5'){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
	
</header>
<div id='dashboard'>
	<?php
		$userModel = new User;
		$refererController = new RefererController;
		$userController = new UserController;
		$refs = $userModel->referer($_SESSION['id']);
		$user = new TransactionController;

		$users = $userModel->getUserDetail($_SESSION['id']);
		extract($users[0]);
	?>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
	<section class='col-xs-12 col-sm-9 col-md-10' style="margin-left:16%">	
<?php
			echo "<div>
			<h3>All Transactions</h4>
			<h5>Paid To</h5>
			<table class='table table-striped'><tr><th>Name</th><th>Level</th><th>Amount Paid</th><th>Date Paid</th><th>Status</th></tr>";
			$pay_level = array("0" => "4", "1" => "16", "2" => "64", "3" => "128");
			$count = 0;
			foreach ($pay_level as $key => $value) {
				$paidby = $user->paidTo($_SESSION['id'], null, null, $key);
				// var_dump($paidby);
				$lm = new LevelModel;
				$level = $lm->getLevel($key)[0]['level'];
				if($level == null) $level = "Starter";
				if($paidby[0] == false) continue;
				echo "<tr><th colspan='6' style=''><span>".$level."</span></th></tr>";
				foreach($paidby[0] as $paid){ 
					if($paid === false) continue;
					extract($paid);
					$stat = ($status == 0) ? "<span class='label label-danger'>Unconfirmed</span>" : "<span class='label label-success'>Confirmed</span>";
					// if($level == null) $level = "Member";
				?>
					<tr>
						<td><?php echo $firstname." ".$lastname ?></td>
						<td><?php echo $level ?></td>
						<td><?php echo $amount ?></td>
						<td><?php echo $date ?></td>
						<td><?php echo $stat ?></td>
					</tr>
				<?php
				// if($count == $value)
				$count++;
				 }
				}
			echo "</table></div><br>";

			echo "<div>
			<h5>Paid By</h5>
			<table class='table table-striped'><tr><th>Name</th><th>Level</th><th>Amount Paid</th><th>Date Paid</th><th>Status</th><th>Action</th></tr>";
			$pay_levels = array("0" => "4", "1" => "16", "2" => "64", "3" => "128");
			$count = 0;
			foreach ($pay_levels as $key => $value) {
				$paidby = $user->paidBy($_SESSION['id'], null, null, $key);
				$lm = new LevelModel;
				$level = $lm->getLevel($key)[0]['level'];
				if($level == null) $level = "Starter";
				if($paidby[0] == false) exit;
				echo "<tr><th colspan='6' style=''><span>".$level."</span></th></tr>";
				foreach($paidby[0] as $paid){ 
					if($paid === false) continue;
					extract($paid);
					$stat = ($status == 0) ? "<span class='label label-danger'>Unconfirmed</span>" : "<span class='label label-success'>Confirmed</span>";
					// if($level == null) $level = "Member";
				?>
					<tr>
						<td><?php echo $firstname." ".$lastname ?></td>
						<td><?php echo $level ?></td>
						<td><?php echo $amount ?></td>
						<td><?php echo $date ?></td>
						<td><?php echo $stat ?></td>
						<td>
							<?php if($status == 0){
								$userModel = new User;
								$getreflevel = $userModel->getUserDetail($paidby);
								extract($getreflevel[0]);
							?>
							<a href='dashboard.php?confirm&paidby=<?php echo urlencode($firstname)." ".urlencode($lastname); ?>&uid=<?php echo $paidby; ?>&tid=<?php echo $paid[0]; ?>&pl=<?php echo $level; ?>' ><button class='btn btn-success'>Confirm</button></a>
							<?php } ?>
						</td>
					</tr>
				<?php
				// if($count == $value)
				$count++;
				 }
				}
			echo "</table></div>";
		?>
</section>
</div>

<?php
	include "view/footer.php";
?>

</body>
</html>