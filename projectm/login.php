<?php 
session_start();
if(isset($_SESSION['id'])) header("location: dashboard.php");
include "forms.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/projectm/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<link rel='stylesheet' href='apps/css/bootstrap.min.css'/>
    <link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<link rel='stylesheet' href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' href='apps/css/main.css'/>
</head>
<body style="background: rgba(0,0,245,0.1)">
<!-- <header >logo</header>-->
<div class='col-md-12' id='logbox'>
<?php if(empty($_GET)){ ?>

<div  style='padding: 20px; /*border: 1px solid #f0f0f0;*/ min-height: 350px;box-shadow: 0px 0px 5px 0px #e7e7e7;background: #fff'>
<a href="/">&larr; Back</a><br>
<a href="/"><img src="apps/images/p5m.jpg" style="width:50%;height:50%;margin-left: 25%;" ></a> <hr>
<form method='POST' action=''>
	<table>
		<tr><td><h3>Login</h3></td></tr>
		<?php 
			if(isset($msg)) foreach($msg as $m) echo "<tr><td>".$m."</td></tr>"; 
			if(isset($_COOKIE["verified"])) echo "<tr><td><p class='alert alert-success'>".$_COOKIE["verified"]."</p></td></tr>";
			if(isset($_COOKIE["verify"])) echo "<tr><td><p class='alert alert-info'>".$_COOKIE["verify"]."</p></td></tr>";
		?>
		<tr>
		<td><input required name='email' type='email' value='<?php if(isset($_POST['email'])){echo $_POST['email'];} ?>' placeholder='Email Address (Required)' title='Ensure you give a valid email address ' /></td>
		</tr>
		<tr>
		<td><input name='password' type='password' value='<?php if(isset($_POST['password'])){echo $_POST['password'];} ?>' placeholder='Password' title='You are advised to use a password of 8 or more character combinations' required /></td>
		</tr>
		<tr>
		<td><br/><input type='submit' name='sign_in' style='background-color: #0080C0; color:#fff' value='Log me in' /></td>
		</tr>
	</table>
</form>
	<hr>
	<span>Not yet a member?</span> 
	<a href='register.php' style='color:#0080C0'><button>Join us</button></a>
	<br>
	<a href='login.php?forgot-password' style='color:#0080C0'><button>Forgot Password?</button></a>
</div>

<?php } 
elseif(isset($_GET['forgot-password'])){ ?>
	<div  style='padding: 20px; /*border: 1px solid #f0f0f0;*/ min-height: 350px;box-shadow: 0px 0px 5px 0px #e7e7e7;background: #fff'>
<a href="login.php">&larr; Back</a><br>
<a href="/"><img src="apps/images/p5m.jpg" style="margin-left: 30%;" ></a> <hr>
<form method='POST' action='<?php echo htmlspecialchars($_SERVER['PHP_SELF'])."?forgot-password";?>'>
	<table>
		<tr><td><h3><span class="mif-event-available"></span>Request Password Change</h3></td></tr>
		<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
		<tr>
		<td><input required name='email' type='email' value='<?php if(isset($_POST['email'])){echo $_POST['email'];} ?>' placeholder='Email Address (Required)' title='Ensure you give a valid email address ' /></td>
		</tr>
		<td><br/>
			<input type='submit' name='forgotpassword' style='background-color: #0080C0; color:#fff' value='Retrieve Password' /></td>
		</tr>
	</table>
</form>
</div>
<?php } elseif(isset($_GET['reset-password']) && isset($_GET['identity'])){ ?>
	<div class="col-sm-12 card" style="min-height: 250px;margin-top:10px;">
	<a href="/">&larr; Back to Home</a><br>
	<a href="/"><img src="apps/images/p5m.jpg" style="width:50%;height:50%;margin-left: 20%;" ></a> <hr>
	<?php if(isset($_GET['email'])){ $email = $_GET['email']; } else $email = $email; ?>
					<h3>Change Password</h3>
					<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?reset-password&identity='.$_GET['identity'].'&email='.$email ?>" method="post">

						<input type="hidden" name="userid" value='<?php echo $_GET['identity']; ?>'>
						<input type="hidden" name="email" value='<?php echo $email; ?>'>
						<table>
							<tr><td><?php if(isset($msg)) foreach($msg as $m) echo $m; ?></td></tr>
							<tr><td><input type="password" name="pass1" placeholder="password"></td></tr>
							<tr><td><input type="password" name="pass2" placeholder="confirm password"></td></tr>
							<tr><td><input type="hidden" name="set"></td></tr>
							<tr><td><button name='password_change' style='background-color: #0080C0; color:#fff'>Change password</button></td></tr>
						</table>
					</form>
				</div>
<?php }
else{ header('location: /');} ?>
</div>
</body>
</html>