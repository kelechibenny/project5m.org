<?php 
session_start();
if(!isset($_SESSION['id'])) header('location: index.php');
include_once "bootstrap/Autoload.php";
date_default_timezone_set('Africa/Lagos');
include "forms.php";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Team</title>
	<meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<base href="/projectm/">
	<script src='apps/scripts/jquery.min.js'></script>
	<script src='apps/scripts/bootstrap.min.js'></script>
	<script src='apps/scripts/metro.min.js'></script>
	<script src="apps/scripts/jquery.datetimepicker.full.min.js"></script>
	<script src='apps/scripts/custom.js'></script>
	<link rel='stylesheet' type="text/css" href='apps/css/bootstrap.min.css'/>
	<link rel="stylesheet" type="text/css" href="apps/css/jquery.datetimepicker.css"/ >
	<!-- <link rel='stylesheet' type="text/css" href='apps/css/metro.min.css'/> -->
    <link rel='stylesheet' type="text/css" href='apps/css/metro-icons.min.css'/>
	<link rel='stylesheet' type="text/css" href='apps/css/main.css'/>
</head>
<body id='dashboard' >
<header >
	<a href='/' style="height:inherit;"><img src='apps/images/p5m.jpg' style="height:inherit;padding-left:10px" /></a> 
	<span class='mif-menu mif-3x' id='nav-menu'></span>
	<?php 
	if(isset($_SESSION['id']) && ($_SESSION['id'] == '1' || $_SESSION['id'] == '2' || $_SESSION['id'] == '3' || $_SESSION['id'] == '4' || $_SESSION['id'] == '5')){ ?>
	<span><a href="admin">Admin Panel</a></span>
	<?php } ?>
</header>
<?php
	$userModel = new User;
	$refererController = new RefererController;
	$userController = new UserController;

	$bankModel = new BankModel;

	$users = $userModel->getUserDetail($_SESSION['id']);
	extract($users[0]);
?>
<div id='dashboard'>
	<aside id='dashboard_panel' class='col-xs-12 col-sm-3 col-md-2' style="">
		<ul>
			<li><img src='<?php echo $dp; ?>' alt='' style='width:30px;height:30px;background: url("apps/images/icon0.png") center;border-radius: 50%;'> <?php echo ucfirst($firstname); ?></li>
			<li><a href="dashboard.php"><span class='mif-apps'></span> Dashboard</a></li>
			<li><a href="profile.php"><span class='mif-profile'></span> Profile</a></li>
			<li><a href="transactions.php"><span class='mif-stack2'></span> Transactions</a></li>
			<li><a href="team.php"><span class='mif-tree'></span> Project Team</a></li>
			<li><a href="promotion.php"><span class='mif-map2'></span> Promotions</a></li>
			<li><a href="contact.php"><span class='mif-ambulance'></span> Support</a></li>
			<li><a href="logout.php"><span class='mif-settings-power'></span> Logout</a></li>
		</ul>
	</aside>
		<section class='col-xs-12 col-sm-9 col-md-10' >
		<h3>&nbsp;&nbsp;My Team</h3><hr>
		<h5 class='well alert-info'>This section is suppose to help you monitor your team members - that is your collaborators, your collaborators' collaborators, down to your fourth generation. You are expected to work with them in reaching your goal, as well as theirs.  </h5>
		<?php 
			$group = $userController->team($_SESSION['id']);
			// echo "<pre>";
			// var_dump($group);
			$count = 0;
			$storeLevels1 =array();

			echo "<h4> First Generation</h4>";
			// $count = 0;
			// var_dump($id);
			echo "<table class='table table-striped'>";
				echo "<tr>";
				echo "<td>Name</td>";
				echo "<td>Email</td>";
				echo "<td>Phone</td>";
				echo "<td>Progress</td>";
				
				echo "<tr>";
			foreach($group[1][$id] as $key => $value){
				$userModel = new User;
				// var_dump(($value));
				$l1 = $userModel->getUserDetail($value);
				extract($l1[0]);
				echo "<tr>";
				echo "<td>".$firstname." ".$lastname."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$phone."</td>";
				echo "<td>";
					echo "<div class='myProgress'>
							  <div class='myBar' id='".$id."'></div>
						</div>";
					echo "<input type='hidden' class='progressValue' id='val".$id."' value='".$wallet."' >";
				echo "</td>";
				echo "<tr>";
				$storeLevels1[$count] = $value;
				$count++;
			}

			echo "</table>";

			echo "<h4> Second Generation</h4>";
			$storeLevels2 =array();
			$count = 0;
			// var_dump($group[2]);
			echo "<table class='table table-striped'>";
				echo "<tr>";
				echo "<td>Referer</td>";
				echo "<td>Name</td>";
				echo "<td>Email</td>";
				echo "<td>Phone</td>";
				echo "<td>Progress</td>";
				
				echo "<tr>";
			foreach($storeLevels1 as $levels){
				foreach($group[2][$levels] as $key => $value){
					$userModel = new User;
					if($value === '0') continue;
					// var_dump(($value));
					$l1 = $userModel->getUserDetail($value);
					extract($l1[0]);
					echo "<tr>";
					echo "<td><span class='label label-info'>".$userModel->getUserDetail($refby)[1]['lastname']."</span></td>";
					echo "<td>".$firstname." ".$lastname."</td>";
					echo "<td>".$email."</td>";
					echo "<td>".$phone."</td>";
					echo "<td>";
					echo "<div class='myProgress'>
							  <div class='myBar' id='".$id."'></div>
						</div>";
					echo "<input type='hidden' class='progressValue' id='val".$id."' value='".$wallet."' >";
					echo "</td>";
					echo "<tr>";
					
					$storeLevels2[$count] = $value;
					$count++;
				}
			}
			

			echo "</table>";

			// var_dump($storeLevels);


			echo "<h4> Third Generation</h4>";
			$storeLevels3 =array();
			$count = 0;
			// var_dump($group[2]);
			echo "<table class='table table-striped'>";
				echo "<tr>";
				echo "<td>Referer</td>";
				echo "<td>Name</td>";
				echo "<td>Email</td>";
				echo "<td>Phone</td>";
				echo "<td>Progress</td>";
				
				echo "<tr>";
			foreach($storeLevels2 as $levels){
				foreach($group[3][$levels] as $key => $value){
					$userModel = new User;
					if($value === '0') continue;
					// var_dump(($value));
					$l1 = $userModel->getUserDetail($value);
					extract($l1[0]);
					echo "<tr>";
					echo "<td><span class='label label-info'>".$userModel->getUserDetail($refby)[1]['lastname']."</span></td>";
					echo "<td>".$firstname." ".$lastname."</td>";
					echo "<td>".$email."</td>";
					echo "<td>".$phone."</td>";
					echo "<td>";
					echo "<div class='myProgress'>
								  <div class='myBar' id='".$id."'></div>
							</div>";
						echo "<input type='hidden' class='progressValue' id='val".$id."' value='".$wallet."' >";
					echo "</td>";
					echo "<tr>";
					
					$storeLevels3[$count] = $value;
					$count++;
				}
			}
			

			echo "</table>";

			// var_dump($storeLevels);


			echo "<h4> Fourth Generation</h4>";
			$storeLevels4 =array();
			$count = 0;
			// echo "<pre>";
			// var_dump($group[4]);
			echo "<table class='table table-striped'>";
				echo "<tr>";
				echo "<td>Referer</td>";
				echo "<td>Name</td>";
				echo "<td>Email</td>";
				echo "<td>Phone</td>";
				echo "<td>Progress</td>";
				
				echo "<tr>";
			foreach($storeLevels3 as $levels){
				foreach($group[4][$levels] as $key => $value){
					$userModel = new User;
					if($value === '0') continue;
					// var_dump(($value));
					$l1 = $userModel->getUserDetail($value);
					extract($l1[0]);
					echo "<tr>";
					echo "<td><span class='label label-info'>".$userModel->getUserDetail($refby)[1]['lastname']."</span></td>";
					echo "<td>".$firstname." ".$lastname."</td>";
					echo "<td>".$email."</td>";
					echo "<td>".$phone."</td>";
					echo "<td>";
					echo "<div class='myProgress'>
							  <div class='myBar' id='".$id."'></div>
							</div>";
						echo "<input type='hidden' class='progressValue' id='val".$id."' value='".$wallet."' >";
					echo "</td>";
					echo "<tr>";
					
					$storeLevels4[$count] = $value;
					$count++;
				}
			}
			

			echo "</table>";

			// var_dump($storeLevels);
			

		?>
	</section>
</div>
	<?php
	// include "view/footer.php";
?>

<script>
$(document).ready(function(){
	$('.myBar').each(function(){
		var item = $(this).attr('id');
		var wallet = $('.progressValue#val'+item+'').val();
		if(wallet >= 5000) {
		var value = (wallet / 5120000) * 100 * 1.0;
			//console.log(value);
			if(item != undefined){
				$('.myBar#'+item+'').animate({
					width : value
				}, "slow")
			}
		}
	});
})
	
</script>

</body>
</html>